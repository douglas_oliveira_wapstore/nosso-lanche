create database tcc;

use tcc;

create table produtos (
	id integer not null primary key auto_increment,
    nome varchar(64) not null,
    descricao varchar (200),
    preco varchar (10) not null,
    tipo varchar(50) not null
);

create table usuario (

	id integer not null primary key auto_increment,
    nome varchar(255) not null,
    email varchar(255) not null unique,
    cpf varchar(30) not null,
    cidade varchar(255),
    estado varchar(255),
    celular varchar(255),
    senha varchar(255) not null,
    tipo varchar(10) not null

);

drop table produtos;

select * from produtos;

select count(*) from produtos;

select * from usuario;

drop table usuario;

update usuario set tipo='admin' where id=4;

update produtos set tipo='lanche' where id=1 or id=2;

ALTER TABLE `produtos` add disponivel char not null;

update produtos set disponivel='s' where id>=1;
