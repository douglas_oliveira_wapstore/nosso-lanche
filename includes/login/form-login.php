<!DOCTYPE html>
<html lang="pt-br">
<head>
    <title>Nosso Lanche</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href = "https://fonts.googleapis.com/css2? family = Comfortaa: wght @ 500 & display = swap" rel = "stylesheet">
    <link rel="stylesheet" type="text/css" href="./../../css/estilo-login.css" />
    <link rel="sortcut icon" href="./../../img/icon.jpg" type="image/x-icon" />
    <link rel="preconnect" href="https://fonts.gstatic.com/" >
    <link href="https://fonts.googleapis.com/css2?family=Comfortaa:wght@500&display=swap" rel="stylesheet">
	<style type="text/css">
        * {
            font-family: Comfortaa, serif;
        }
    </style>
    

    <style type="text/css">
        * {
            overflow: visible;
        }
        
        body {
            display: grid;
            grid-template-areas: "login cadastro";
        }
        #menu {
            grid-column-start: login; 
            grid-column-end: cadastro;
        }
            
        
    </style>
    <script type="text/javascript">
        function mm(){
            var senha=document.getElementById("senha");
            if(senha.type=="password"){
                senha.type="text";
            }
            else{
                senha.type="password";
            }
        }
    </script>

<!--

// //FRASE DE BOAS-VINDAS AO USUÁRIO/VISITANTE
// $fraseHeader = '';

// if(Login::isLogged()) {
//     $fraseHeader = 'Olá, '.$_SESSION['usuario']['nome'].'. <a href="index.php?logout="><strong>Sair</strong></a>';
// }
// else {
//     $fraseHeader = 'Olá, visitante. <a href="login.php"><strong>Entrar</strong></a>';
// }

?>     -->

</head>
 <nav id="menu">
    <ul>
        <div class="tit-nav"><li  style="border-left: 1px solid black; margin-top: -5px;"><a href="./../home/index.php"><img  src="./../../img/casa.png" alt="some text" width=30 height=30 ></a></li></div>
        <div class="tit-nav"><li><a href="./../comprar/comprar.php">COMPRAR</a></li></div>
        <div class="tit-nav"><li><a href="./../contato/contato.php">CONTATO</a></li></div>
        <div class="tit-nav-exception tit-nav"><li style="color: white;font-size: 13px;">Olá, visitante. <strong><a href='#'> Entrar </a></strong></li></div>
      
    </ul>
</nav>

    
<body>
   

        <div>
    
        <br></br>
         <form method="post" class="form">
         <h1> LOGIN </h1>
        
         <?=$alertaLogin?>
         <br>
         <label id="email">E-mail:</label>
         <input type="email" name="email" class="content" value="<?=isset($_POST['acao'])&&$_POST['acao']=='logar' ? $_POST['email'] ?? '' : ''?>" required >
         <label>Senha:</label>
         <input type="password" name="senha" class="content" id="senha" value="<?=isset($_POST['acao'])&&$_POST['acao']=='logar' ? $_POST['senha'] ?? '' : ''?>" required>
         <input class="conteno" type="checkbox" id="olho" onclick="mm()" /><p class="ver">Mostrar senha</p>
         <button type="submit" id="cadastrar" name="acao" value="logar">Entrar</button>
         <br>
         <a class="forgot-passwd-link" href="./../../includes/redefine-senha/form-redefinir-senha.html">Esqueci a senha</a>
     </form>
 </div> 

    <div>
        
        <br>
         <form method="post" class="form">
         <h1> CADASTRAR </h1>
         <?=$alertaEmail?>
         <?=$alertaSenha?>
         <label id="nome">Nome:</label>
         <input class="content" name="nome" type="text" id="cnome" size="70" maxlength="60" value="<?=isset($_POST['acao'])&&$_POST['acao']=='cadastrar' ? $_POST['nome'] ?? '' : ''?>" required/>
         <label id="email">E-mail:</label>
         <input type="email" name="email" class="content" value="<?=isset($_POST['acao'])&&$_POST['acao']=='cadastrar' ? $_POST['email'] ?? '' : ''?>" required>
         <label id="cpf">C.P.F.:</label>
         <input class="content" name="cpf" type="text" id="ccpf" size="70" maxlength="60" value="<?=isset($_POST['acao'])&&$_POST['acao']=='cadastrar' ? $_POST['cpf'] ?? '' : ''?>" required/>	
         <label id="cidade">Cidade:</label>
         <input class="content" name="cidade" type="text" id="ccidade" value="Birigui" placeholder="Birigui" readonly="true"size="70" maxlength="60" required/>	
         <label id="estado">Estado:</label>
         <input class="content" name="estado" type="text" id="cestado" value="São Paulo" placeholder="São Paulo" readonly="true"size="70" maxlength="60" required="" />	
         <label id="tel">Celular:</label>
         <input class="content" name="celular" type="text" id="ccelular" size="70" maxlength="60" placeholder="(xx) xxxxx-xxxx" value="<?=isset($_POST['acao'])&&$_POST['acao']=='cadastrar' ? $_POST['celular'] ?? '' : ''?>"  required/>
         <label>Senha:</label>
            <input type="password" name="senha" class="content" id="senha" value="<?=isset($_POST['acao'])&&$_POST['acao']=='cadastrar' ? $_POST['senha'] ?? '' : ''?>" required>
 
         <label> Confirme sua Senha:</label>
            <input type="password" name="senha2" class="content" id="senha" value="<?=isset($_POST['acao'])&&$_POST['acao']=='cadastrar' ? $_POST['senha2'] ?? '' : ''?>" required>
         <input class="conteno" type="checkbox" id="olho" onclick="mm()" /><p class="ver">Mostrar senha</p>
         <button type="submit" id="cadastrar" name="acao" value="cadastrar">Cadastrar dados preenchidos</button>
         <input name="limpar" type="reset" id="limpar" value="Limpar Campos preenchidos!" /></p>
     </form>
 </div>	
</body>
</html>