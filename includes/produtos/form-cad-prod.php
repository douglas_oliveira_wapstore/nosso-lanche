<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" type="text/css" href="./../../css/estilo.css">
    
    <title>Nosso lanche</title>

    <link rel="preconnect" href="https://fonts.gstatic.com/" >
    <link href="https://fonts.googleapis.com/css2?family=Comfortaa:wght@500&display=swap" rel="stylesheet">
	<style type="text/css">
        * {
            font-family: Comfortaa, serif;

        }
        body {
            background-image: url("../../img/lanche.jpg");
	background-repeat: no-repeat;
	background-size: 100%;
        }

        label {color: white;}
    </style>
    

</head>
<body>    	
	<nav id="menu">
			<ul>
				<div class="tit-nav"><li  style="border-left: 1px solid black; margin-top: -5px;"><a href="./../home/index.php"><img  src="./../../img/casa.png" alt="some text" width=30 height=30 ></a></li></div>
				<div class="tit-nav"><li><a href="#">PEDIDOS</a></li></div>
				<div class="tit-nav"><li><a href="#">COMPRAR</a></li></div>
				<div class=""><li><a href="produtos.php" class="active">PRODUTOS</a></li></div>
				<div class="tit-nav"><li><a href="./../usuarios/usuarios.php">USUÁRIOS</a></li></div>
				<div class="tit-nav"><li><a href="#">MENSAGENS</a></li></div>

				<div class="tit-nav-exception tit-nav"><li style="color: white;"><?=$welcome?></li></div>
				
			</ul>
	</nav>
    
    <main>
        <div class="content-admin" style="margin-top: 10px; background-color: rgb(0,0,0,0.61);">
            <div class="titulo">
                <h2> <?=$titulo?> </h2>
            </div>
            <br>
            <br>    
            <div class="form-content">
                <form method="post" class="form">
                    <label for="nome">Nome:</label><br>
                    <input type="text" class="content text-input-form-prod" name="nome" value="<?=$obProd->nome ?? ''?>" required><br><br>  

                    <label for="preco">Preço:</label><br>
                    <input type="text" class="content text-input-form-prod" name="preco" style="width: 200px;" value="<?=$obProd->preco ?? ''?>" required>
                    
                    <label for="quantidade-unidade">a cada:</label>
                    <input type="number" class="content text-input-form-prod" style="width: 100px;" name="quantidade_unidade" value="<?=$obProd->quantidade_unidade ?? ''?>" required>

                    <select name="unidade" class="content" style="width: 125px;">
                        <option value="" selected disabled>Selecione</option>
                        <option value="unidade" <?=(isset($obProd)&&$obProd->unidade == 'unidade') ? 'selected' : ''?>>unidade</option>
                        <option value="grama" <?=(isset($obProd)&&$obProd->unidade == 'grama') ? 'selected' : ''?>>grama</option>
                        <option value="kilo" <?=(isset($obProd)&&$obProd->unidade == 'kilo') ? 'selected' : ''?>>kilo</option>
                        <option value="mililitro" <?=(isset($obProd)&&$obProd->unidade == 'mililitro') ? 'selected' : ''?>>mililitro</option>
                        <option value="litro" <?=(isset($obProd)&&$obProd->unidade == 'litro') ? 'selected' : ''?>>litro</option>
                    </select>

                    <br> <br>

                    <label for="descricao">Descrição</label> <br>
                    <textarea name="descricao" cols="30" rows="10" class="content" style="height: 150px; padding-top: 10px; resize:none;"><?=$obProd->descricao ?? ''?></textarea> <br> <br>

                    
                    <label for="tipo">Tipo:</label><br>
                    <select name="tipo" id="" class="content" required>
                        <option value="" selected disabled>Selecione uma opção</option>
                        <option value="lanche" <?=(isset($obProd)&&$obProd->tipo == 'lanche') ? 'selected' : ''?>>Lanche</option>
                        <option value="bebida" <?=(isset($obProd)&&$obProd->tipo == 'bebida') ? 'selected' : ''?>>Bebida</option>
                        <option value="batata" <?=(isset($obProd)&&$obProd->tipo == 'batata') ? 'selected' : ''?>>Batata recheada</option>
                        <option value="aditivo_lanche" <?=(isset($obProd)&&$obProd->tipo == 'aditivo_lanche') ? 'selected' : ''?>>Adicional de Lanche</option>
                        <option value="aditivo_batata" <?=(isset($obProd)&&$obProd->tipo == 'aditivo_batata') ? 'selected' : ''?>>Adicional Batata</option>
                        <option value="outro" <?=(isset($obProd)&&$obProd->tipo == 'outro') ? 'selected' : ''?>>Outro</option>
                    </select>
                    <br>
                    <br>

                    <input type="radio" name="disponivel" value="s" required <?=$obProd->disponivel == 's' ? 'checked' : ''?>> Disponível
                    <input type="radio" name="disponivel" value="n" required <?=$obProd->disponivel == 'n' ? 'checked' : ''?>> Indisponível

                    <br>
                    <br>

                    <button type="submit" id="cadastrar" name="cadastrar" style="float:left;"><?=$btn?></button>
                    
                    <a href="produtos.php"><button type="button" id="limpar">Cancelar</button></a>

                </form>
            </div>
        </div>
    </main>


	</body>
</html>