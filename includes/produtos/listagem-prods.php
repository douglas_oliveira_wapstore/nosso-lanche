<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" type="text/css" href="./../../css/estilo.css">
    
	<title>Home</title>

	<link rel="preconnect" href="https://fonts.gstatic.com/" >
    <link href="https://fonts.googleapis.com/css2?family=Comfortaa:wght@500&display=swap" rel="stylesheet">
	<style type="text/css">
        #menu {
            font-family: Comfortaa, serif;
        }
    </style>

	
<?php 

$listagem = '';
$contadorclass = 0;

foreach($obProds as $prod) {
	$contadorclass++;
	//OS NÚMEROS PARES DE REGISTROS DA TABELA FICARÃO MAIS CLARO
	$class = ($contadorclass % 2 == 0) ? 'listagem-claro' : 'listagem-escuro';

	$listagem .= '<tr class="'.$class.'">
					<td>'.$prod->id.'</td>
					<td>'.$prod->nome.'</td>
					<td>'.$prod->preco.'<br>/'.$prod->quantidade_unidade.' '.$prod->unidade.'</td>
					<td>'.$prod->descricao.'</td>
					<td style="text-transform: capitalize;">'.$prod->tipo.'</td>
					<td>'.(($prod->disponivel == 's') ? 'Sim' : 'Não') .'</td>
					<td><a href="editar-prod.php?id=' . $prod->id . '"><button type="button" class="btn-primary">Editar</button></a>
						<a href="excluir-prod.php?id='.$prod->id.'"><button type="button" class="btn-danger">Excluir</button></a>
					</td>
				</tr>';
}

	$listagem = strlen($listagem) ? $listagem : '<tr class="listagem-claro"> <td colspan=7 height="100px;"> Nenhum produto encontrado </td> </tr>';
	
	
    $message = '';

    //EXIBE MENSAGEM DE SUCESSO E ERRO NO HEADER
    if(isset($_GET['status'])) {
        switch ($_GET['status']) {
            case 'success':
                $message = '<div class="alert-success">Ação executada com sucesso!</div>';
                break;
            
            case 'error';
                $message = '<div class="alert alert-danger"> Houve um erro.</div>';
        }
    }
    
	$pagination = '';

	unset($_GET['pagina']);
	$query = http_build_query($_GET);
	
	//MOSTRA OS BOTÕES DA PAGINAÇÃO
	foreach($pages as $page) {
		$classli= ($page['atual']) ? 'pagination-active-li' : '';
		$classa = ($page['atual']) ? 'pagination-active-a' : '';

		$pagination.= '<a class="'.$classa.'" href="produtos.php?pagina='.$page['pagina'].'&'.$query.'"><li class="'.$classli.'">'.$page['pagina'].'</li></a>'; 
	}

	//--------------------BOTÕES DE PRÓXIMO E ANTERIOR ----------------------------//
	$proximo = '';
	$anterior = '';
	$classproximo='';
	$classanterior='';


	//MOSTRA OS BOTÕES DE' PRÓXIMO E ANTERIOR' DA PAGINAÇÃO
	foreach($pages as $page) {
		if($page['atual']) {
			$proximo = $page['pagina'] + 1;
			$anterior = $page['pagina'] -1;

			$keysPages = array_keys($pages);
			$lastKeyPages = end($keysPages);

			//SE A PRÓXIMA PÁGINA FOR MAIOR DO QUE O NÚMERO DA PÁGINA DO ÚLTIMO ARRAY
			if($proximo > $pages[$lastKeyPages]['pagina']) {
				$classproximo = 'pagination-disabled-p';
			}
			if($anterior < 1) {
				$classanterior = 'pagination-disabled-a';
			}
		}

		$previous = '<a class="'.$classproximo.'" href="produtos.php?pagina='.$proximo.'&'.$query.'"><li class="'.$classproximo.'">></li></a>';
		$back = '<a class="'.$classanterior.'" href="produtos.php?pagina='.$anterior.'&'.$query.'"><li class="'.$classanterior.'"><</li></a>';
	}

	//---------------------BOTÕES DE ÚLTIMO E PRIMEIRO ---------------//
	foreach($pages as $page) {
		if($page['atual']) {
			$ultimo = $obPagination->pages;
			$primeiro = 1;

			//SE A PRÓXIMA PÁGINA FOR MAIOR DO QUE O NÚMERO DA PÁGINA DO ÚLTIMO ARRAY
			
		$last = '<a href="produtos.php?pagina='.$ultimo.'&'.$query.'"><li>>></li></a>';
		$first = '<a href="produtos.php?pagina='.$primeiro.'&'.$query.'"><li><<</li></a>';
		}

	}
    ?>
</head>

<body>    	
	<nav id="menu">
			<ul>
				<div class="tit-nav"><li  style="border-left: 1px solid black; margin-top: -5px;"><a href="./../home/index.php"><img  src="./../../img/casa.png" alt="some text" width=30 height=30 ></a></li></div>
				<div class="tit-nav"><li><a href="#">PEDIDOS</a></li></div>
				<div class="tit-nav"><li><a href="./../comprar/comprar.php">COMPRAR</a></li></div>
				<div class=""><li><a href="#" class="active">PRODUTOS</a></li></div>
				<div class="tit-nav"><li><a href="./../usuarios/usuarios.php">USUÁRIOS</a></li></div>

				<div class="tit-nav-exception tit-nav"><li style="color: white;"><?=$welcome?></li></div>
				
			</ul>
	</nav>
    
    <main>
        <center>
            <div class="content-listagem">
                <a href="cadastro-prod.php"><button type="button" class="btn-cadastro">Cadastrar produto</button></a>
			</div>
			<?=$message?>
			<div class="listagem">
				<form method='get'>
					<input type="number" name="id" class="text-filtro" placeholder='ID' value="<?=(isset($id)) ? $id : ''?>">
					<input type="text" name="nome" class="text-filtro" placeholder='Filtre pelo nome' style="width: 30vw;" value="<?=(isset($nome)) ? $nome : ''?>">
					<select name="tipo" class="text-filtro" style="width: 10vw;">
						<option value="" <?=(isset($tipo) && $tipo == '') ? 'selected' : ''?>>Todos os tipos</option>
						<option value="lanche" <?=(isset($tipo) && $tipo == 'lanche') ? 'selected' : ''?>>Lanche</option>
						<option value="bebida" <?=(isset($tipo) && $tipo == 'bebida') ? 'selected' : ''?>>Bebida</option>
						<option value="batata" <?=(isset($tipo) && $tipo == 'batata') ? 'selected' : ''?>>Batata recheada</option>
						<option value="aditivo_lanche" <?=(isset($obProd)&&$obProd->tipo == 'aditivo_lanche') ? 'selected' : ''?>>Aditivos de lanche</option>
						<option value="aditivo_batata" <?=(isset($obProd)&&$obProd->tipo == 'aditivo_batata') ? 'selected' : ''?>>Aditivos de batata</option>
						<option value="outros" <?=(isset($tipo) && $tipo == 'outros') ? 'selected' : ''?>>Outros</option>
					</select>

					<select name="disponivel" class="text-filtro" style="width: 10vw;">
						<option value="" <?=(isset($disponivel) && $disponivel == '') ? 'selected' : ''?>>Disp./Indisp.</option>
						<option value="s" <?=(isset($disponivel) && $disponivel == 's') ? 'selected' : ''?>>Disponível</option>
						<option value="n" <?=(isset($disponivel) && $disponivel == 'n') ? 'selected' : ''?>>Indisponível</option>
					</select>
					<button type="submit" name='filtrar' class='btn-primary' style='padding: 10px;'>Filtrar</button>
				</form>
				<div class="table-listagem">
					<table cellspacing="0" class="table-listagem">
						<tr>
							<th width="50px">ID</th>
							<th width="150px">Nome</th>
							<th width="120px">Preço</th>
							<th width="350px">Descrição</th>
							<th width="100px">Tipo</th>
							<th width="50px">Disponível</th>
							<th width="120px">Ação</th>
						</tr>
						<?=$listagem?>
					</table>
				</div>
			</div>
				<div class="div-pagination">
					<ul class="pagination">
						<?=$first?>
						<?=$back?>
						<?=$pagination?>
						<?=$previous?>
						<?=$last?>
					</ul>
				</div>
        </center>
    </main>


	</body>
</html>