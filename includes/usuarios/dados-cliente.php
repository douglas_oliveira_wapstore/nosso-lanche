<!DOCTYPE html>
<html>
<head>
<link rel="stylesheet" type="text/css" href="./../../css/estilo.css">
    
	<link rel="preconnect" href="https://fonts.gstatic.com/" >
    <link href="https://fonts.googleapis.com/css2?family=Comfortaa:wght@500&display=swap" rel="stylesheet">
	<style type="text/css">
        #menu {
            font-family: Comfortaa, serif;
        }

        .titulo h2 {
            font-family: Comfortaa, serif;
        }
    </style>
    
    <title>Nosso lanche</title>
    

</head>
<body>    	
	<nav id="menu">
			<ul>
				<div class="tit-nav"><li  style="border-left: 1px solid black; margin-top: -5px;"><a href="./../home/index.php"><img  src="./../../img/casa.png" alt="some text" width=30 height=30 ></a></li></div>
				<div class="tit-nav"><li><a href="#">PEDIDOS</a></li></div>
				<div class="tit-nav"><li><a href="#">COMPRAR</a></li></div>
				<div class=""><li><a href="produtos.php" class="active">PRODUTOS</a></li></div>
				<div class="tit-nav"><li><a href="#">USUÁRIOS</a></li></div>

				<div class="tit-nav-exception tit-nav"><li style="color: white;"><?=$welcome?></li></div>
				
			</ul>
	</nav>
    
    <main>
        <div class="content-admin" style="margin-top: 10px; padding-bottom: 20px;">
            <div class="titulo">
                <h2> Dados do usuário </h2>
            </div>
            <br>
            <br>    
            <div class="form-content">
                    <label id="nome">Nome:</label><br>
                    <input class="content" name="nome" type="text" id="cnome" size="70" maxlength="60" value='<?=$obUser->nome?>' disabled required/><br><br>
                    <label id="email">E-mail:</label><br>
                    <input type="email" name="email" class="content" value="<?=$obUser->email?>" disabled required><br><br>
                    <label id="cpf">C.P.F.:</label><br>
                    <input class="content" name="cpf" type="text" id="ccpf" value="<?=$obUser->cpf?>" size="70" maxlength="60" disabled required/>	<br><br>
                    <label id="cidade">Cidade:</label><br>
                    <input class="content" name="cidade" type="text" id="ccidade" value="Birigui" placeholder="Birigui" readonly="true"size="70" maxlength="60" disabled required/><br><br>
                    <label id="estado">Estado:</label><br>
                    <input class="content" name="estado" type="text" id="cestado" value="São Paulo" placeholder="São Paulo" readonly="true"size="70" maxlength="60" disabled required="" /><br>	<br>
                    <label id="tel">Celular:</label><br>
                    <input class="content" name="celular" type="text" id="ccelular" size="70" maxlength="60" value='<?=$obUser->celular?>' placeholder="(xx) xxxxx-xxxx" disabled required/><br><br>

        
                    <a href="usuarios.php"><button type="button" id="limpar" style="width: 100px;">Voltar</button></a>

            </div>
        </div>
    </main>


	</body>
</html>