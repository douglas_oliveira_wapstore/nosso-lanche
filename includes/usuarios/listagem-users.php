<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" type="text/css" href="./../../css/estilo.css">
    
	<link rel="preconnect" href="https://fonts.gstatic.com/" >
    <link href="https://fonts.googleapis.com/css2?family=Comfortaa:wght@500&display=swap" rel="stylesheet">
	<style type="text/css">
        #menu {
            font-family: Comfortaa, serif;
        }
    </style>

	<title>Home</title>

	
<?php 

	$listagem = '';

	// print_r($users);
	// exit;
	$contadorclass = 0;

	foreach($users as $user) {
			$contadorclass++;

			//OS NÚMEROS PARES DE REGISTROS DA TABELA FICARÃO MAIS CLARO
			$class = ($contadorclass % 2 == 0) ? 'listagem-claro' : 'listagem-escuro';

			//GERENCIA O BOTÃO DE TROCAR O TIPO DO USUÁRIO
			$msg = ($user->tipo == 'mod') ? 'Tornar comum' : 'Tornar moderador';

			$listagem .= '<tr class="'.$class.'">
							<td>'. $user->id . '</td>
							<td>'.$user->nome.'</td>
							<td>'.$user->email.'</td>
							<td>'.$user->celular.'</td>
							<td>'.date('d/m/Y à\s H:i:s',strtotime($user->datacadastro)).'</td>
							<td>'.$user->tipo.'</td>
							<td><a href="mostrar-user.php?id=' . $user->id . '"><button type="button" class="btn-primary">Ver</button></a>
							
							';

			//CASO O USUÁRIO SEJA ADMIN, NÃO SERÁ PERMITIDO EXCLUI-LO NEM ALTERAR SUA FUNÇÃO
			if ($user->tipo !== 'admin') {
				$listagem .= '		
							<div class="acao">
								<button type="button" class="btn-danger">Ações &nbsp;<img src="https://image.flaticon.com/icons/png/512/25/25623.png" height="10px" alt=""></button>
								<div class="sub-acao">
										<a href="usuarios.php?change='.$user->tipo.'&id='.$user->id.'">'.$msg.'</a>
										<a href="excluir-user.php?id='.$user->id.'">Excluir</a>
								</div>

							</td>
							</tr>';

			
										}
			else {
				$listagem .= '</td></tr>';
			}
		}

	$listagem = strlen($listagem) ? $listagem : '<tr class="listagem-claro"> <td colspan=7 height="100px;"> Nenhum usuário encontrado </td> </tr>';
	
	
    $message = '';

    //EXIBE MENSAGEM DE SUCESSO E ERRO NO HEADER
    if(isset($_GET['status'])) {
        switch ($_GET['status']) {
            case 'success':
                $message = '<div class="alert-success">Ação executada com sucesso!</div>';
                break;
            
            case 'error';
				$message = '<div class="alert alert-danger"> Houve um erro.</div>';
				break;
				
        }
    }
    
	$pagination = '';

	unset($_GET['pagina']);
	$query = http_build_query($_GET);
	
	//MOSTRA OS BOTÕES DA PAGINAÇÃO
	foreach($pages as $page) {
		$classli= ($page['atual']) ? 'pagination-active-li' : '';
		$classa = ($page['atual']) ? 'pagination-active-a' : '';

		$pagination.= '<a class="'.$classa.'" href="usuarios.php?pagina='.$page['pagina'].'&'.$query.'"><li class="'.$classli.'">'.$page['pagina'].'</li></a>'; 
	}

	//--------------------BOTÕES DE PRÓXIMO E ANTERIOR ----------------------------//
	$proximo = '';
	$anterior = '';
	$classproximo='';
	$classanterior='';

	//MOSTRA OS BOTÕES DE' PRÓXIMO E ANTERIOR' DA PAGINAÇÃO
	foreach($pages as $page) {
		if($page['atual']) {
			$proximo = $page['pagina'] + 1;
			$anterior = $page['pagina'] -1;

			$keysPages = array_keys($pages);
			$lastKeyPages = end($keysPages);

			//SE A PRÓXIMA PÁGINA FOR MAIOR DO QUE O NÚMERO DA PÁGINA DO ÚLTIMO ARRAY
			if($proximo > $pages[$lastKeyPages]['pagina']) {
				$classproximo = 'pagination-disabled-p';
			}
			if($anterior < 1) {
				$classanterior = 'pagination-disabled-a';
			}
		}

		$previous = '<a class="'.$classproximo.'" href="usuarios.php?pagina='.$proximo.'&'.$query.'"><li class="'.$classproximo.'">></li></a>';
		$back = '<a class="'.$classanterior.'" href="usuarios.php?pagina='.$anterior.'&'.$query.'"><li class="'.$classanterior.'"><</li></a>';
	}

	//---------------------BOTÕES DE ÚLTIMO E PRIMEIRO ---------------//
	foreach($pages as $page) {
		if($page['atual']) {
			$ultimo = $obPagination->pages;
			$primeiro = 1;

			//SE A PRÓXIMA PÁGINA FOR MAIOR DO QUE O NÚMERO DA PÁGINA DO ÚLTIMO ARRAY
			
		$last = '<a href="usuarios.php?pagina='.$ultimo.'&'.$query.'"><li>>></li></a>';
		$first = '<a href="usuarios.php?pagina='.$primeiro.'&'.$query.'"><li><<</li></a>';
		}

	}

    ?>
</head>

<body>    	


	<nav id="menu">
			<ul>
				<div class="tit-nav"><li  style="border-left: 1px solid black; margin-top: -5px;"><a href="./../home/index.php"><img  src="./../../img/casa.png" alt="some text" width=30 height=30 ></a></li></div>
				<div class="tit-nav"><li><a href="#">PEDIDOS</a></li></div>
				<div class="tit-nav"><li><a href="./../comprar/comprar.php">COMPRAR</a></li></div>
				<div class=""><li><a href="./../produtos/produtos.php">PRODUTOS</a></li></div>
				<div class="tit-nav"><li><a href="#" class="active">USUÁRIOS</a></li></div>

				<div class="tit-nav-exception tit-nav"><li style="color: white;"><?=$welcome?></li></div>
				
			</ul>
	</nav>
    
    <main>
        <center>
			<?=$message?>
			<div class="listagem" style="margin-top: 20px;">
				<form method='get'>
					<input type="number" name="id" class="text-filtro" placeholder='ID' value="<?=$id ?? ''?>">
					<input type="text" name="nome" class="text-filtro" placeholder='Filtre pelo nome' style="width: 30vw;" value="<?=$nome ?? ''?>">
					<input type="text" name="telefone" class="text-filtro" placeholder='Telefone' style="width: 100px" value="<?=$telefone ?? ''?>">
					<select name="tipo" class="text-filtro" style="width: 10vw;">
						<option value="" selected>Todos os tipos</option>
						<option value="comum" <?=isset($tipo)&&$tipo=='comum' ? 'selected' : ''?>>Comum</option>
						<option value="mod" <?=isset($tipo)&&$tipo=='mod' ? 'selected' : ''?>>Moderador</option>
						<option value="admin" <?=isset($tipo)&&$tipo=='admin' ? 'selected' : ''?>>Administrador</option>
					</select>
						
					<button type="submit" name='filtrar' class='btn-primary' style='padding: 10px;'>Filtrar</button>
				</form>
				<div class="table-listagem">
					<table cellspacing="0" class="table-listagem">
						<tr>
							<th width="50px">ID</th>
							<th width="150px">Nome</th>
							<th width="120px">Email</th>
							<th width="80px">Telefone</th>
							<th width="100px">Data de cadastro</th>
							<th width="50px">Tipo</th>
							<th width="120px">Ações</th>
						</tr>
						<?=$listagem?>
					</table>
				</div> 
			</div>
			<div class="div-pagination">
					<ul class="pagination">
						<?=$first?>
						<?=$back?>
						<?=$pagination?>
						<?=$previous?>
						<?=$last?>
					</ul>
				</div>
        </center>
    </main>


	</body>
</html>