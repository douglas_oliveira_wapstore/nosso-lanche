<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" type="text/css" href="./../../css/estilo.css">
    
	<title>Nosso lanche</title>

	<link rel="preconnect" href="https://fonts.gstatic.com/" >
    <link href="https://fonts.googleapis.com/css2?family=Comfortaa:wght@500&display=swap" rel="stylesheet">
	<style type="text/css">
        * {
            font-family: Comfortaa, serif;
        }
    </style>

</head>

<body>    	
	<nav id="menu">
			<ul>
				<div class="tit-nav"><li  style="border-left: 1px solid black; margin-top: -5px;"><a href="./../home/index-admin.php"><img  src="./../../img/casa.png" alt="some text" width=30 height=30 ></a></li></div>
				<div class="tit-nav"><li><a href="#">PEDIDOS</a></li></div>
				<div class="tit-nav"><li><a href="./../comprar/comprar.php">COMPRAR</a></li></div>
				<div class=""><li><a href="produtos.php" class="active">PRODUTOS</a></li></div>
				<div class="tit-nav"><li><a href="./../usuarios/usuarios.php">USUÁRIOS</a></li></div>

				<div class="tit-nav-exception tit-nav"><li style="color: white;"><?=$welcome?></li></div>
				
			</ul>
	</nav>
    
    <main>
        <center>
            <div class="listagem">
                <form method="post">
                    <h1>Deseja excluir</h1> <h1 style="color:  #faaf3e"><?=$obUser->nome?> ?</h1>
                    <br>
                    <button type="submit" name="remover" class="btn-danger" style="padding: 15px;">Excluir</button>
                    <a href="usuarios.php"><button type="button" class="btn-primary" style="padding:15px;">Cancelar</button></a>
                </form>
            </div>
        </center>
    </main>


	</body>
</html>