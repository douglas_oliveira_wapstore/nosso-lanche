<script>
                    // Get the modal
                    var modal<?=$contador?> = document.getElementById("myModal<?=$contador?>");

                    // Get the button that opens the modal
                    var btn<?=$contador?> = document.getElementById("myBtn<?=$contador?>");

                    // Get the <span> element that closes the modal
                    var span<?=$contador?> = document.getElementsByClassName("close<?=$contador?>")[0];

                    // When the user clicks the button, open the modal
                    btn<?=$contador?>.onclick = function() {
                      modal<?=$contador?>.style.display = "block";
                    }

                    // When the user clicks on <span> (x), close the modal
                    span<?=$contador?>.onclick = function() {
                      modal<?=$contador?>.style.display = "none";
                    }

                    // When the user clicks anywhere outside of the modal, close it
                    window.onclick = function(event) {
                      if (event.target == modal<?=$contador?>) {
                        modal<?=$contador?>.style.display = "none";
                      }
                    }

                    </script>