<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="preconnect" href="https://fonts.gstatic.com/" >
    <link rel="stylesheet" type="text/css" href="./../../css/estilo.css">
    <link href="https://fonts.googleapis.com/css2?family=Comfortaa:wght@500&display=swap" rel="stylesheet">
    <title>Nosso lanche</title>
    <link rel="preconnect" href="https://fonts.gstatic.com/" >
    <link href="https://fonts.googleapis.com/css2?family=Comfortaa:wght@500&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="./../../css/modal.css">
    <style type="text/css">
        * {
            font-family: Comfortaa, serif;
        }
    </style>
    <?php
    
    
    use \App\Entity\Produto;
    use \App\Entity\Carrinho;
    
    $message = '';
    
    if(isset($_GET['status'])) {
        switch ($_GET['status']) {
            case 'success':
            $message = '<div class="alert-success">Ação executada com sucesso!</div>';
            break;
            
            case 'error';
            $message = '<div class="alert alert-danger"> Houve um erro.</div>';
        }
    }

    
                                ?>
                            </head>
                            
                            <body>
                                <nav id="menu">
                                    <ul>
                 <div class="tit-nav"><li  style="border-left: 1px solid black; margin-top: -5px;"><a href="./../home/index.php"><img  src="./../../img/casa.png" alt="some text" width=30 height=30 ></a></li></div>
                 <?php if($obUser->tipo=='admin'): ?>
                 <div class="tit-nav"><li><a href="#">PEDIDOS</a></li></div>
                 <?php endif;?>
                 <div class="tit-nav"><li><a href="#"  class="active">COMPRAR</a></li></div>

                 <?php if($obUser->tipo =='admin'): ?>
                 <div class=""><li><a href="./../produtos/produtos.php">PRODUTOS</a></li></div>
                 <div class="tit-nav"><li><a href="./../usuarios/usuarios.php">USUÁRIOS</a></li></div>

                 <?php endif; 
                 
                 if($obUser->tipo == 'comum'): ?>
                 

                 <div class="tit-nav"><li><a href="./../contato/contato.php">CONTATO</a></li></div>

                <?php endif; ?>
                                        
                                        
                                        <div class="tit-nav-exception tit-nav"><li style="color: white;"><?=$welcome?></li></div>
                                        
                                        
                                    </ul>
                                    <div class="tit-nav" id="carrinho"><a href="#"><img src="" alt="" width="30px"></a></div>
                                </nav>
                                
                                <main>
                                    <center>
                                        <?=$message?>
                                        <div class="listagem">
                                            
                                            <?php
                                            
                                            //CONTADOR PARA AS CLASSES
                                            $contador = 0;
                                            
                                            
                                            //VERIFICA SE HÁ ALGUM PRODUTO NO CARRINHO
                                            if (sizeof($_SESSION['carrinho']['produtos']) !== 0){
                                            
                                            //MOSTRA OS PRODUTOS COMPRADOS ATÉ ENTÃO
                                            foreach($_SESSION['carrinho']['produtos'] as $key=>$produto) {
                                            
                                            $contador++;
                                            
                                            //OBJETO DO PRODUTO
                                            
                                            $obProduto = Produto::getprodbyid($produto['id']);

                                            echo '
                                            
                                            <!-- TÍTULO DO PRODUTO -->
                                            <h1 class="titulo-produto">'.$produto['quantidade'] . ' ' . $produto['nome'].'</h1><br>

                                            <!-- TOTAL DO PRODUTO -->
                                            <p class="total-produto" style="color: #faaf3e!important; font-size: 21px;">R$ '. (number_format($produto['quantidade']*$produto['valorunit'], 2, '.', '')+$_SESSION['carrinho']['produtos'][Carrinho::SearchProd($produto['id'])]['valoradicionais']).'</p>

                                            <!-- UNIDADE DO PRODUTO -->
                                            <p class="unidade-produto" style="font-size: 14px;color: #ddd;">'.(in_array($obProduto->tipo, ['lanche', 'batata']) ? ('R$'.($produto['valortotal']-$_SESSION['carrinho']['produtos'][Carrinho::SearchProd($produto['id'])]['valoradicionais']).(($obProduto->tipo == 'lanche') ? ' dos lanches' : ' das batatas')) : '').'</p>

                                            '.(in_array($obProduto->tipo, ['lanche','batata']) ? ('<p class="unidade-produto" style="font-size: 14px; color: #ddd;">+ R$'.$_SESSION['carrinho']['produtos'][Carrinho::SearchProd($produto['id'])]['valoradicionais'].' dos adicionais </p></br>'): '').'

                                            



                                            <!-- EXCLUIR O PRODUTO -->
                                            <a href="carrinho.php?acaoproduto=excluir&keyproduto='.$key.'"><button type="button" class="btn-outline-danger">Excluir</button></a>
                                            
                                            <!-- BOTÃO DE INSERIR ADICIONAIS -->';



                                            //ARRAY QUE SERÁ COLOCADO NO FOREACH PARA MOSTRAR OS ADITIOS    
                                            $array = (($obProduto->tipo == 'batata') ? $obaditivo_batata : $obaditivo_lanche);

                                            

                                            //VARIÁVEL QUE ABRIGA O TIPO QUE O FOREACH IRÁ ANALISAR PARA DISPOR OS ADITIVOS NA TELA
                                            if(in_array($obProduto->tipo, ['lanche', 'batata'])) {

                                            echo '
                                            <button type="button" id="myBtn'.$contador.'" class="btn-outline-primary">Inserir adicionais</button>';

                                            }

                                            echo '
                                            
                                            <!-- MODAL QUE MOSTRA OS ADICIONAIS -->
                                            <div id="myModal'.$contador.'" class="modal">
                                                
                                                <!-- Modal content -->
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <span class="close'.$contador.'">&times;</span>
                                                        <h2>Adicionais</h2>
                                                    </div>
                                                    <div class="modal-body" >
                                                        <form method="post" action="escolher-produto.php">';

                                                    $contadorproduto = 1;

                                                    
                                                        //ESTRUTURA CONDICIONAL QUE IRÁ MOSTRAR UM BOTÃO PARA CADA PRODUTO ADICIONADO AO CARRINHO
                                                         for($i = 1; $i <= $produto['quantidade']; $i++){


                                                            echo '<div class="dropdown'.$contador.'">
                                                                    <button type="button" class="dropbtn'.$contador.'" style="margin-bottom: 2px;">'.$produto['nome'].' '.$contadorproduto.'</button>
                                                                    <div class="dropdown-content'.$contador.'">';

                                                            
                                                            foreach($array as $adicional) {

                                                                        //VALUE QUE SALVA AS QUANTIDADES DOS ADICIONAIS SETADAS NA LISTA
                                                            
                                                                        $value = ($_SESSION['carrinho']['produtos'][Carrinho::SearchProd($produto['id'])]['adicionais']['Produto'.$contadorproduto][$adicional->id]) ?? null;


                                                                       echo '<a><label for="numero"><input type="number" value="'.$value.'" name="Produto'.$contadorproduto.'['.$adicional->id.']" style="width: 40px;" min="0">'.$adicional->nome.'(+'.$adicional->preco.')</label></a>';

                                                            }

                                                            



                                                                   echo' </div>
                                                                </div>'; 
                                                            
                                                                include __DIR__.'./css/dropdown.php'; 

                                                            $contadorproduto++;
                                                         }

                                                    echo '

                                                        
                                                     </div>
                                                    <div class="modal-footer">
                                                        
                                                    <button type="submit" name="enviaradicional" class="btn-primary" style="padding: 10px;" value="'.$produto['id'].'">Enviar</button>
                                                    </form>
                                                    
                                                    </div>
                                                </div>
                                                
                                            </div>';
                                            
                                            

                                            


                                            echo '     
                                            <hr width="30%" style="margin-top: 20px;margin-bottom:20px;">';

                                            include __DIR__.'./css/modal.php';
                                            include __DIR__.'./js/modal.php';
                                            
                                            
                                            }



                                            echo '
                                            
                                            <p style="margin-bottom: 40px; color: white;"><b>Total: R$</b> '.number_format($_SESSION['carrinho']['valortotal'], 2, '.','').'</p>';
                                            
                                        }
                                     
                                            
                                            if(sizeof($_SESSION['carrinho']['produtos']) == 0): ?>
                                            <p style="color: white; margin-bottom: 40px;"> Nenhum produto inserido no carrinho.<br><br> <a href="comprar.php" style="color: #faaf3e!important;">Clique aqui</a> para iniciar a compra</p>
                                            <?php 
                                            
                                            endif;



                                            if(sizeof($_SESSION['carrinho']['produtos'])!==0):
                                            
                                            ?>


                                            
                                            <a href="comanda.php"><button type="button" class="btn-primary" style="padding: 15px;">Prosseguir com a compra</button></a>
                                            
                                            <a href="comprar.php"><button type="button" class="btn-primary" style="padding: 15px;">Adicionar produto</button></a>

                                            <a href="carrinho.php?cancela_carrinho="><button type="button" class="btn-danger" style="padding: 15px;">Cancelar compra</button></a>

                                            
                                            <?php endif; ?>
                                            
                                        </div>
                                    </center>
                                </main>
                                
                            </body>
                            </html>
                            