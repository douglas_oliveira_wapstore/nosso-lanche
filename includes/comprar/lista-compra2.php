<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" type="text/css" href="./../../css/estilo.css">
    <link rel="stylesheet" type="text/css" href="./../../css/comprar.css">
		<link rel="preconnect" href="https://fonts.gstatic.com/" >
    <link href="https://fonts.googleapis.com/css2?family=Comfortaa:wght@500&display=swap" rel="stylesheet">
	<style type="text/css">
        * {
            font-family: Comfortaa, serif;
        }
    </style>

	<title>Home</title>


<?php


    $message = '';

    if(isset($_GET['status'])) {
        switch ($_GET['status']) {
            case 'success':
                $message = '<div class="alert-success">Ação executada com sucesso!</div>';
                break;

            case 'error';
                $message = '<div class="alert alert-danger"> Houve um erro.</div>';
        }
    }

    $listagem = '';

    foreach($obProds as $produto){
		$listagem .= "<form method='post'>
						<div class='container-prod'>
                           <div class='container-prod-tit'> ".$produto->nome."</div>
                           <div class='container-prod-preco'> ".$produto->preco."</div>
						   <div class='container-prod-descricao'>".$produto->descricao."</div>
						   <div class='container-prod-adicionar'><input type='number' value='1' class='quantidadeprod' placeholder='Quantidade' name='quantidade' required min='1' max='50'><button type='submit' class='btn-bordered-yellow' name='produto' value='".$produto->id."'>Adicionar ao carrinho</button></div>
					  	</div>
					  </form>";
	}
	

    

	// $pagination = '';

	// unset($_GET['pagina']);
	// $query = http_build_query($_GET);

	// //MOSTRA OS BOTÕES DA PAGINAÇÃO
	// foreach($pages as $page) {
	// 	$classli= ($page['atual']) ? 'pagination-active-li' : '';
	// 	$classa = ($page['atual']) ? 'pagination-active-a' : '';

	// 	$pagination.= '<a class="'.$classa.'" href="produtos.php?pagina='.$page['pagina'].'&'.$query.'"><li class="'.$classli.'">'.$page['pagina'].'</li></a>';
	// }

	//--------------------BOTÕES DE PRÓXIMO E ANTERIOR ----------------------------//
	// $proximo = '';
	// $anterior = '';
	// $classproximo='';
	// $classanterior='';


	// //MOSTRA OS BOTÕES DE' PRÓXIMO E ANTERIOR' DA PAGINAÇÃO
	// foreach($pages as $page) {
	// 	if($page['atual']) {
	// 		$proximo = $page['pagina'] + 1;
	// 		$anterior = $page['pagina'] -1;

	// 		//SE A PRÓXIMA PÁGINA FOR MAIOR DO QUE O NÚMERO DA PÁGINA DO ÚLTIMO ARRAY
	// 		if($proximo > $pages[array_key_last($pages)]['pagina']) {
	// 			$classproximo = 'pagination-disabled-p';
	// 		}
	// 		if($anterior < 1) {
	// 			$classanterior = 'pagination-disabled-a';
	// 		}
	// 	}

	// 	$previous = '<a class="'.$classproximo.'" href="produtos.php?pagina='.$proximo.'&'.$query.'"><li class="'.$classproximo.'">></li></a>';
	// 	$back = '<a class="'.$classanterior.'" href="produtos.php?pagina='.$anterior.'&'.$query.'"><li class="'.$classanterior.'"><</li></a>';
	// }

    ?>
</head>

<body>
	<nav id="menu">
			<ul>
			<div class="tit-nav"><li  style="border-left: 1px solid black; margin-top: -5px;"><a href="./../home/index.php"><img  src="./../../img/casa.png" alt="some text" width=30 height=30 ></a></li></div>
                 <?php if($obUser->tipo=='admin'): ?>
                 <div class="tit-nav"><li><a href="#">PEDIDOS</a></li></div>
                 <?php endif;?>
                 <div class="tit-nav"><li><a href="#"  class="active">COMPRAR</a></li></div>

                 <?php if($obUser->tipo =='admin'): ?>
                 <div class=""><li><a href="./../produtos/produtos.php">PRODUTOS</a></li></div>
                 <div class="tit-nav"><li><a href="./../usuarios/usuarios.php">USUÁRIOS</a></li></div>

                 <?php endif; 
                 
                 if($obUser->tipo == 'comum'): ?>
                 

                 <div class="tit-nav"><li><a href="./../contato/contato.php">CONTATO</a></li></div>

                <?php endif; ?>


				<div class="tit-nav-exception tit-nav"><li style="color: white;"><?=$welcome?></li></div>


			</ul>
			<div class="tit-nav" id="carrinho"><a href="#"><img src="" alt="" width="30px"></a></div>
	</nav>

    <main>
        <center>
			<?=$message?>
			
			<div class="listagem">
                	<?=$listagem?>
			</div>
        </center>
    </main>


	</body>
</html>
