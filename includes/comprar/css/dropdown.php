<style>

<?php echo '.dropbtn'.$contador;?> {
  background-color: #7a7a7a;
  color: white;
  padding: 16px;
  font-size: 16px;
  border: none;
  cursor: pointer;
  width: 400px;
  outline: none;
}

<?php echo '.dropdown'.$contador;?> 
{
  position: relative;
  display: inline-block;
}

<?php echo '.dropdown-content'.$contador;?> {
  display: none;
  position: absolute;
  background-color: #faaf3e;
  min-width: 160px;
  box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
  z-index: 1;
  width: 400px;
}


<?php echo '.dropdown-content'.$contador;?> a {
  color: black;
  padding: 12px 16px;
  text-decoration: none;
  display: block;
}


<?php echo '.dropdown-content'.$contador;?> a:hover {background-color: #f5a834;}

<?php echo '.dropdown'.$contador;?>:focus-within <?php echo '.dropdown-content'.$contador;?> {
  display: block;
}

<?php echo '.dropdown'.$contador;?>:hover <?php echo '.dropbtn'.$contador;?> {
  background-color: #6d6969;
}

</style>
