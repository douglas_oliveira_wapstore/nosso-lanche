<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="preconnect" href="https://fonts.gstatic.com/" >
     <link rel="stylesheet" type="text/css" href="../../css/estilo.css">
    <link rel="stylesheet" type="text/css" href="../../css/comanda-css.css">
    <link href="https://fonts.googleapis.com/css2?family=Comfortaa:wght@500&display=swap" rel="stylesheet">
    <title>Nosso lanche</title>
    <link rel="preconnect" href="https://fonts.gstatic.com/" >
    <link href="https://fonts.googleapis.com/css2?family=Comfortaa:wght@500&display=swap" rel="stylesheet">


    <?php 

        use \App\Entity\Produto;
    

        $listagemcarrinho = '';

        foreach($produtos_carrinho as $produto) {

        //OBJETO DO PRODUTO
        $obProduto = Produto::getprodbyid($produto['id']);
        
        $listagemcarrinho .= '
        <tr>
            <td>
                '.$produto["nome"].'
            </td>
            <td>
                '.(($obProduto->quantidade_unidade)*$produto['quantidade']) . ' '. (($produto['quantidade'] > 1) ? $obProduto->unidade . 's' : $obProduto->unidade).'
            </td>
            <td>
                R$' . $produto['valorunit'].'
            </td>
            <td>
                R$'. ($produto['valorunit'] * $produto['quantidade']) . '
            </td>
        </tr>
        ';
        
        }



        $listagemadicionais = '';

        foreach($produtos_carrinho as $produto) {

            if($produto['adicionais'] == null) {
                $produto['adicionais'] = array();
            }

            foreach($produto['adicionais'] as $listagemadicional){

                $contador = 0;
                foreach($listagemadicional as $adicional=>$quantidade) {
                
                $contador++;

                $obAdicional = Produto::getprodbyid($adicional);
                
                 $listagemadicionais .= '
                     <tr>'.(($contador == 1) ? '
                         <td rowspan='.count($listagemadicional).'>
                             '.$produto['nome'].'
                         </td>' : '').'
                         <td>
                             '.$obAdicional->nome.'
                         </td>
                         <td>
                             '.(((int)$obAdicional->quantidade_unidade * (int)$quantidade) .' ' . (($quantidade > 1) ? $obAdicional->unidade . 's' : $obAdicional->unidade)).'
                         </td>
                         <td>
                             R$'.number_format(str_replace("R$", "", str_replace(",",".",str_replace(" ","",$obAdicional->preco))), 2, '.', '')*$quantidade.'
                         </td>
                     </tr>
              
        
                 ';
                }
            }

        }

    ?>

	<style type="text/css">
        * {
            font-family: Comfortaa, serif;
        }
    </style>


    </head>

     <body>
         <nav id="menu">
             <ul>
             <div class="tit-nav"><li  style="border-left: 1px solid black; margin-top: -5px;"><a href="./../home/index.php"><img  src="./../../img/casa.png" alt="some text" width=30 height=30 ></a></li></div>
                 <?php if($obUser->tipo=='admin'): ?>
                 <div class="tit-nav"><li><a href="#">PEDIDOS</a></li></div>
                 <?php endif;?>
                 <div class="tit-nav"><li><a href="#"  class="active">COMPRAR</a></li></div>

                 <?php if($obUser->tipo =='admin'): ?>
                 <div class=""><li><a href="./../produtos/produtos.php">PRODUTOS</a></li></div>
                 <div class="tit-nav"><li><a href="./../usuarios/usuarios.php">USUÁRIOS</a></li></div>

                 <?php endif; 
                 
                 if($obUser->tipo == 'comum'): ?>
                 

                 <div class="tit-nav"><li><a href="./../contato/contato.php">CONTATO</a></li></div>

                <?php endif; ?>

                 <div class="tit-nav-exception tit-nav"><li style="color: white;"><?=$welcome?></li></div>
                  <div class="tit-nav" id="carrinho"><a href="#"><img src="" alt="" width="30px"></a></div>
             </ul>
         </nav>
        <br>
        <br>
    <div class="conteudo">
        <br><h1>Comanda</h1><br>
          <table class="tabelinha">
            <tr>
                <th>Nome</th>
                <th>Unidades</th>
                <th>Preço unit.</th>
                <th>Preço total</th>

            </tr>
            <?=$listagemcarrinho?>
          </table>
          <br>
           <table class="tabelinha">

            <tr>
                <th>Produto</th>
                <th>Adicional</th>
                <th>Quantidade</th>
                <th>Valor</th>
            </tr>
            
            <?=$listagemadicionais?>


           </table>

           <br>

           <table class="tabelinha">
            <tr>
                <th>Sub-total</th>
                <th>Taxa de entrega</th>
                <th>Total</th>

            </tr>
            <tr>
                <td>R$<?=number_format(str_replace("R$", "", str_replace(",",".",str_replace(" ","",$_SESSION['carrinho']['valortotal']))), 2, '.', '')?></td>
                <td>R$ 3.00</td>
                <td>R$<?=number_format(str_replace("R$", "", str_replace(",",".",str_replace(" ","",$_SESSION['carrinho']['valortotal']))), 2, '.', '') + 3.00?></td>
            </tr>
          </table>
          <br>
          <form class="form-comanda" action="pdf-generate.php" method="post">

          <h2 style="color: white; text-align: center;">Informações adicionais</h2> <br>

        <textarea name="informacoes_adicionais"  class="content" id="" cols="30" rows="80" style="width: 50vw; min-height: 100px; padding: 10px;" placeholder="Informe detalhes do seu pedido, por ex: 'Um X-Salada sem tomate'"></textarea>

          <div class="pagamento">
          <h2 style="color: white; text-align: center;"> Dados de pagamento </h2> <br>


                    <script type="text/javascript">

            function changeFunc() {
                var selectBox = document.getElementById("selectBox");
                var selectedValue = selectBox.options[selectBox.selectedIndex].value;
                

                if(selectedValue == 'dinheiro') {
                    document.getElementById('dados').style.display = 'inline';
                    document.getElementById('dados2').style.display = 'none';

                }

                else if(selectedValue == 'dinheiro_cartao') {
                    document.getElementById('dados').style.display = 'inline';
                    document.getElementById('dados2').style.display = 'inline';
                }

                else {
                    document.getElementById('dados').style.display = 'none';
                    document.getElementById('dados2').style.display = 'none';
                }
            }

            </script>



          <select class="content input-comanda" name="pagamento" id="selectBox" onchange="changeFunc();" style="width: 50.5vw;" required>

            <option value="" disabled selected >
                Forma de pagamento:
            </option>
            <option value="dinheiro">Dinheiro</option>
            <option value="cartao">Cartão</option>
            <option value="dinheiro_cartao">Dinheiro e cartão</option>



            </select>
            <br>

            <input type="number" min="1" step=".01" name="valor_dinheiro" class="content input-comanda" id="dados2" placeholder="Valor que será pago no dinheiro: " style="width: 50.5vw; display: none; margin-top: 10px;">

            <input type="text" name="troco" class="content input-comanda" id="dados" placeholder="Troco para: " style="width: 50.5vw; display: none; margin-top: 10px;">


          </div>

           <br>
            <div class="entrega" style="margin-top: 20px; text-align: center;">
                <h2 style="color: white; text-align: center;"> Dados da entrega </h2>
                <br>

                    <input type="text" name="endereco" class="content input-comanda" style="width:40vw" placeholder="Insira o endereço:" required>
                    <input type="number" name="numero_house" class="content input-comanda" style="width: 10vw;" placeholder="Número da casa: " maxlenght="9999" minlenght="1" required><br><br>
                    <select class="content input-comanda" name="bairro" style="width: 50.5vw;" required>

                        <option value="" disabled selected>
                            Selecione o Bairro:
                        </option>
                        <option value="recanto_verde">Recanto Verde</option>
                        <option value="portal1">Portal 1</option>
                        <option value="portal2">Portal 2</option>
                        <option value="portaldoparque">Portal do Parque</option>
                        <option value="parquedompedro">Parque Dom Pedro</option>

                    </select>
                    <br>
                    <br>
                    <input type="text" name="complemento" class="content input-comanda" style="width: 50.5vw;" placeholder="Complemento">

                    <br>
                    <br>
                    <br>

                    <button type="submit" class="btn-primary" style="padding: 10px;">Finalizar compra</button>
                    <a href="carrinho.php"><button type="button" class="btn-danger" style="padding: 10px;">Voltar para o carrinho</button></a>
                </form>

            
        </div>
    </div>
       </body>
    </html>
