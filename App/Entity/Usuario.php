<?php 


namespace App\Entity;

use \App\Db\Database;
use \PDO;

class Usuario {


    /**
     * Identificador único do usuário
     * @var integer
     */
    public $id;

    /**
     * Nome do usuário
     * @var string
     */
    public $nome;

    /**
     * Email do usuário
     * @var string
     */
    public $email;

    /**
     * CPF do usuário
     * @var string
     */
    public $cpf;

    /**
     * Cidade do usuário
     * O valor foi, por pré-definição, dado como "Birigui", uma vez que o estabelecimento atende apenas nessa área geográfica"
     * @var string
     */
    public $cidade;

    /**
     * Estado do usuário
     * O valor foi, por pré-definição, dado como "São Paulo", uma vez que o estabelecimento atende apenas nessa área geográfica"
     * @var string
     */
    public $estado;

    /**
     * Número de telefone do usuário
     * @var string 
     */
    public $celular;

    /**
     * Senha do usuário
     * @var string
     */
    public $senha;

    /**
     * Tipo do usuário
     * @var string in_array['comum', 'moderador', 'admin']
     */

     public $tipo;

     /**
      * Data e horário de cadastro do usuário
      * @var date
      */
    public $date;

    public function cadastrar() {

        //CONEXÃO COM O BANCO
        $obDatabase = new Database('usuario2');

        $this->date = date('Y-m-d H:i:s');

        //ARRAY COM OS VALORES A SEREM INSERIDOS NO BANCO
        $values = [
            //  o tipo estabelece o grau de hierarquia que o 
            //usuário irá ocupar no sistema, se é administrador ou usário comum
            'tipo' => $this->tipo,
            'nome' => $this->nome,
            'email' =>$this->email,
            'cpf'  => $this->cpf,
            'cidade' => $this->cidade,
            'estado'=> $this->estado,
            'celular'=>$this->celular,
            'senha' => $this->senha,
            'datacadastro' => $this->date
        ];

        //CONEXÃO COM O BANCO PARA REALIZAR A INSERÇÃO DE DADOS
        $this->id = $obDatabase->insert($values);

        //RETORNA SUCESSO
        return true;




    }

    /**
     * Função responsável por atualizar dados do banco, baseado no id do usuário
     * 
     */
    public function atualizar($id) {

            return (new Database('usuario2'))->update('id ='.$id, [
                'tipo' => $this->tipo,
                'nome' => $this->nome,
                'email' =>$this->email,
                'cpf'  => $this->cpf,
                'cidade' => $this->cidade,
                'estado'=> $this->estado,
                'celular'=>$this->celular,
                'senha' => $this->senha,
                'datacadastro' => $this->date
            ]);
    }

    /**
     * Função responsável por retornar os dados de um usuário, pelo seu ip
     * @param string $where
     * @param string $order
     * @param string $limit
     * @return PDO
     */

    public static function getUser($where = null, $order = null, $limit = null) {
        return (new Database('usuario2'))->select($where, $order, $limit)->fetchObject(self::class); //fetchObject(qua objeto será utilizado para representar os resultados de uma consulta)
    }

    /**
     * Função responsável por retornar todos os usuários do banco
     * @param string $where
     * @param string $order
     * @param string $limit
     * @return PDO
     */
    public static function getUsers($where=null, $order=null, $limit=null, $fields = '*') {
        return (new Database('usuario2'))->select($where, $order, $limit, $fields)->fetchAll(PDO::FETCH_CLASS, self::class);
    }


    /**
       * Função responsável por retornar a quantidade de usuários que existem no banco de dados+
       * @return PDO
       * @param string $where
       */
      public static function getQuantidadeUsers($where) {
        return (new Database('usuario2'))->select($where, null, null, 'count(*) as qtd')->fetchObject(self::class)->qtd;
      }

      
      /**
       * Função responsável por remover usuários do banco de dados
       */

      public static function excluir($id) {
        return(new Database('usuario2'))->delete('id ='.$id);
      }


}

?>