<?php 

namespace App\Entity;
use \App\Db\Database;
use \PDO;

class Produto {


    /**
     * Identificador do produto
     * @var integer
     */
    public $id;

    /**
     * Nome do produto
     * @var string
     */
    public $nome;

    /**
     * Descrição do produto
     * @var string
     */
    public $descricao;

    /**
     * 
     * Preço do produto
     * @var string
     */
    public $preco;

    /**
     * Tipo de produto
     * @var string
     */
    public $tipo;

    /**
     * define se o produto está disponível para ser vendido
     * @var string
     */
    public $disponivel;

    /**
     * Define o valor da unidade de medida que será posteriormente determinada na variável abaixo
     * @var integer
     */
    public $quantidade_unidade;

    /**
     * Define a unidade de medida do produto a ser vendido
     */
    public $unidade;

    /**
     * Função responsável por realizar o cadastro do produto no banco de dados
     */

     public function cadastrar() {
      //OBJETO DO BANCO DE DADOS
        $obDatabase = new Database('produtos');

      //VARIÁVEL QUE ABRIGA OS DADOS A SEREM INSERIDOS NO BANCO
      $values = [
         'nome' => $this->nome,
         'descricao'=>$this->descricao,
         'preco' =>$this->preco,
         'tipo'=>$this->tipo,
         'disponivel'=>$this->disponivel,
         'quantidade_unidade' =>$this->quantidade_unidade,
         'unidade' => $this->unidade
      ];

      // cadastra o produto e atribui a ele o identificador
        $this->id=$obDatabase->insert($values);

        //RETORNA SUCESSO
      return true;
     }

     /**
      * Função responsável por alterar os dados do banco de dados
      */
      public function atualizar($id) {
        //REALIZA A CONEXÃO COM O BANCO DE DADOS
        $obDatabase = new Database('produtos');

        //ARRAY CONTENDO OS VALORES A SEREM ALTERADOS
        $values = [
          'nome' => $this->nome,
          'descricao'=>$this->descricao,
          'preco' =>$this->preco,
          'tipo'=>$this->tipo,
          'disponivel'=>$this->disponivel,
          'quantidade_unidade'=>$this->quantidade_unidade,
          'unidade'=>$this->unidade
        ];

        // REALIZA A ATUALIZAÇÃO DOS DADOS
         $obDatabase->update('id = '.$id, $values);

         //RETORNA SUCESSO
         return true;
      }

      /**
       * Função responsável por remover produtos do banco de dados
       */

       public static function excluir($id) {
         return(new Database('produtos'))->delete('id ='.$id);
       }

     /**
      * Função responsável por obter todos os produtos do banco de dados
      * @param string $where
      * @param string $order
      * @param string $limit
      * @return PDO
      */
      public static function getProds($where = null, $order = null, $limit = null) {
        return (new Database('produtos'))->select($where, $order, $limit)->fetchAll(PDO::FETCH_CLASS, self::class);
      }

      /**
       * Função responsável por retornar determinado produto, baseado em seu id
       * @param integer $id
       * @return PDO
       */
      public static function getprodbyid($id) {
        return (new Database('produtos'))->select('id ='.$id)->fetchObject(self::class);
      }

      /**
       * Função responsável por retornar a quantidade de produtos que existem no banco de dados+
       * @return PDO
       * @param string $where
       */
      public static function getQuantidadeProdutos($where) {
        return (new Database('produtos'))->select($where, null, null, 'count(*) as qtd')->fetchObject(self::class)->qtd;
      }


}


?>