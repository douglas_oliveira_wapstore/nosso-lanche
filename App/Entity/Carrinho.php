<?php 
        //MOLDE DE LÓGICA DO CARRINHO
                //   $_SESSION['carrinho'] = array(
                //       'produtos' => [
                //           [0] => [
                //               'nomeprod' => 'X-Salada',
                //               'quantidade'  => 2,
                //               'preco' => 12.00,
                //               'adicionais' => [
                //                                 [0] => [
                //                                         [0] => [
                //                                             'id' => 4,
                //                                             'nome' => 'Peperone',
                //                                             'valor' => 12.00
                //                                         ],

                //                                         [1] => [
                //                                             'id' => 3,
                //                                             'nome' => 'Hambúrguer',
                //                                             'valor' => 50.00
                //                                         ]
                //                                     ],

                //                                 [1] =>    [
                //                                         [0] => [
                //                                             'id' => 4,
                //                                             'nome' => 'Peperone',
                //                                             'valor' => 12.00
                //                                         ],

                //                                         [1] => [
                //                                             'id' => 3,
                //                                             'nome' => 'Hambúrguer',
                //                                             'valor' => 50.00
                //                                         ]
                //                                     ]
                //                     ]
                //                 ],



                //          [1] => [
                //              'nomeprod'=>'X-Bacon',
                //               'quantidade'=> 3,
                //              'preco' => 12.00,
                //              'adicionais' => []
                //          ],
                //          [2] => [
                //              'nomeprod' => 'X-Bacon',
                //               'quantidade' => 4,
                //              'preco' => 15.00,
                //              'adicionais' => []
                //          ]
                //          ]
                //          );

    namespace App\Entity;

    use \App\Entity\Produto;

    class Carrinho {


        /**
         * Função responsável por iniciar a sessão do carrinho
         */
        private function init() {
            if (session_status() !== PHP_SESSION_ACTIVE) {
                session_start();
            }

        }

        public static function iniciarCarrinho() {
            self::init();

            if(!isset($_SESSION['carrinho'])) {



                //ESTABELECE A SESSÃO DO CARRINHO,CASO AINDA NÃO ESTEJA INICIADA
                $_SESSION['carrinho'] = [
                    'produtos' => [],
                    'valortotal' => 0
                ];
            }
        }


        /**
         * Função responsável por buscar produtos na sessão e returnar a chave deles
         * @param integer $id
         * @return integer
         */
        public static function SearchProd($id) {

            //BUSCA O PRODUTO NA SESSÃO
            foreach($_SESSION['carrinho']['produtos'] as $key => $produto) {
                if ($id == $produto['id']) return $key;
            }

            //RETORNA NULO, CASO NENHUM PRODUTO SEJA ENCONTRADO
            return null;

        }

        /**
         * Função responsável por adicionar produtos no carrinho
         * @param integer $id
         * @param integer $quantity
         */
        public static function addProd($id, $quantity) {
            //INICIA A SESSÃO
            self::init();

            //VERIFICA SE JÁ HÁ UM CARRINHO INICIADO
            self::iniciarCarrinho();

            //LOCALIZA O PRODUTO NO BANCO DE DADOS
            $obProd = Produto::getprodbyid($id);

            //VERIFICA AS INFORMAÇÕES DA URL
            if(!$obProd instanceof Produto || !isset($quantity) || !isset($id) ||!is_numeric($quantity)) {
                header('location: comprar.php?status=error');
                exit;
            }

            //VARIÁVEL QUE RESGATA UM PRODUTO EXISTENTE NA SESSÃO
            $keyproduto = self::SearchProd($id);

            //VERIFICA SE JÁ HÁ ALGUM PRODUTO NA SESSÃO
            if(is_null($keyproduto)){


                    //ADICIONA O NOVO PRODUTO À SESSÃO
                    $_SESSION['carrinho']['produtos'][] = [
                        'id' => $obProd->id,
                        'nome' => $obProd->nome,
                        'valorunit' => $valor = number_format(str_replace("R$", "", str_replace(",",".",str_replace(" ","",$obProd->preco))), 2, '.', ''),
                        'quantidade' => $quantity,
                        'valortotal' => $total = $valor * $quantity,
                        'adicionais' => null,
                        'valoradicionais' => 0
                    ];

                    //ACRESCENTA O VALOR TOTAL NA SESSÃO
                    $_SESSION['carrinho']['valortotal'] += $total;

                                //REDIRECIONA PARA O CARRINHO
                        header('location: carrinho.php?status=success');
                        exit;
            }
            //SE JÁ HÁ O PRODUTO NA SESSÃO, ELE SERÁ SOBRESCRITO
            else {

                self::OverwriteProd($keyproduto, $quantity);
    
            }

        }

            /** Método responsável por sobrescrever um produto já contido no carrinho */

            private static function OverwriteProd($keyproduto,$quantity) {

                //VARIÁEL QUE RESGATA UM PRODUTO DA SESSÃO, BASEADO EM SUA CHAVE
                $base = $_SESSION['carrinho']['produtos'][$keyproduto];

                //VARIÁVEL QUE ACRESCENTA A QUANTIDADE DO PRODUTO NA SESSÃO
                $replacement_quantidade = ['quantidade' => $quantidade = ($_SESSION['carrinho']['produtos'][$keyproduto]['quantidade'] + $quantity)];

                //VARIÁVEL QUE ACRESCENTA O VALOR TOTAL DO PRODUTO NA SESSÃO
                $replacement_valortotal_prod =[ 'valortotal' => $quantidade * $_SESSION['carrinho']['produtos'][$keyproduto]['valorunit']];

                //REALIZA A SOBRESCRITA DOS DADOS NA SESSÃO
                $_SESSION['carrinho']['produtos'][$keyproduto] = array_replace($base, $replacement_quantidade, $replacement_valortotal_prod);

                //SOBRESCREVE O VALOR TOTAL DOS PRODUTOS NO CARRINHO
                $_SESSION['carrinho']['valortotal'] += $quantity * $_SESSION['carrinho']['produtos'][$keyproduto]['valorunit'];

                //APAGA A VARIÁVEL DE PRODUTO DA SESSÃO
                unset($_POST['produto']);
                
                //REDIRECIONA PARA O CARRINHO COM O COMANDO DE SUCESSO
                header('location: carrinho.php?status=success');
                exit;
            }


        /**
         * Método responsável por excluir um produto da sessão
         * @param $key (chave do produto no array session)
         */

         public static function deleteProd($key) {

            //SUBTRAI O VALOR TOTAL DO CARRINHO
             $_SESSION['carrinho']['valortotal'] -= $_SESSION['carrinho']['produtos'][$key]['valortotal'];

            //DESATIVA O PRODUTO DA SESSÃO
            unset($_SESSION['carrinho']['produtos'][$key]);


             //REDIRECIONA PARA O CARRINHO
             header('location: carrinho.php?status=success');
             exit;
         }

         /**
          * Método responsável por adicionar aditivos aos produtos
          */
          public static function addAdicional($post, $idprod) {


            //CANCELA O VALOR DO BOTÃO ENVIAR ADICIONAL, PARA QUE SEJA ADICIONADO NA SESSÃO APENAS OS ADICIONAIS
            unset($post['enviaradicional']);

            //FILTRA CADA VALOR
            foreach($post as $key=>$adicionais) {
                $post[$key] = array_filter($adicionais);
            }

            //FILTRA OS PRODUTOS CUJOS ADICIONAIS SÃO NULOS
            $post = array_filter($post);

            //BUSCA O PRODUTO NA SESSÃO
            $produto  = $_SESSION['carrinho']['produtos'][self::SearchProd($idprod)];

            //VARIÁVEL QUE ABRIGA OS ADICIONAIS
            $adicionais = $produto['adicionais'];

            //VERIFICA SE HÁ VALOR DENTRO DO ARRAY DE ADICIONAIS
            if(sizeof($_SESSION['carrinho']['produtos'][self::SearchProd($idprod)]['adicionais']) == 0) {

                //CASO NÃO HAJA, SERÃO ADICIONADOS OS VALORES NÃO NULOS DO ARRAY
                $_SESSION['carrinho']['produtos'][self::SearchProd($idprod)]['adicionais'] = array_filter($post);


                //CASO NÃO HAJA VALORES DENTRO DOS ADICIONAIS, SERÃO ADICIONADOS OS NOVOS INGREDIENTES E 
                //ACRESCENTADOS NO VALOR TOTAL SEUS RESPECTIVOS VALORES
                    foreach ($post as $lanche) {
                        foreach($lanche as $key=>$value) {

                        //BUSCA O ADICIONAL NO BANCO DE DADOS
                        $obProd = Produto::getprodbyid($key);
    
                        //AUMENTA OU DIMINUI O VALOR TOTAL DO CARRINHO
                        $_SESSION['carrinho']['valortotal'] +=  number_format(str_replace("R$", "", str_replace(",",".",str_replace(" ","",$obProd->preco))), 2, '.', '') * (int)$value;
    
                        //AUMENTA OU DIMINUI O VALOR TOTAL DO PRODUTO JUNTAMENTE COM OS ADICIONAIS
                        $_SESSION['carrinho']['produtos'][self::SearchProd($idprod)]['valortotal'] += (number_format(str_replace("R$", "", str_replace(",",".",str_replace(" ","",$obProd->preco))), 2, '.', '')*(int)$value);
    
                        //AUMENTA OU DIMINUI O VALOR TOTAL DOS ADICIONAIS NO PRODUTO
                        $_SESSION['carrinho']['produtos'][self::SearchProd($idprod)]['valoradicionais'] += (number_format(str_replace("R$", "", str_replace(",",".",str_replace(" ","",$obProd->preco))), 2, '.', '')*(int)$value);
    
                        }
                    }
            }

            else {


                //DIMINUI TODOS OS VALORES DE CADA ADICIONAL INSERIDO NO CARRINHO
                foreach ($_SESSION['carrinho']['produtos'][self::SearchProd($idprod)]['adicionais'] as $lanche) {
                    foreach($lanche as $key=>$value) {

                    //BUSCA O ADICIONAL NO BANCO DE DADOS
                    $obProd = Produto::getprodbyid($key);

                    //AUMENTA OU DIMINUI O VALOR TOTAL DO CARRINHO
                    $_SESSION['carrinho']['valortotal'] -=  number_format(str_replace("R$", "", str_replace(",",".",str_replace(" ","",$obProd->preco))), 2, '.', '') * $value;

                    //AUMENTA OU DIMINUI O VALOR TOTAL DO PRODUTO JUNTAMENTE COM OS ADICIONAIS
                    $_SESSION['carrinho']['produtos'][self::SearchProd($idprod)]['valortotal'] -= (number_format(str_replace("R$", "", str_replace(",",".",str_replace(" ","",$obProd->preco))), 2, '.', '')*$value);

                    //AUMENTA OU DIMINUI O VALOR TOTAL DOS ADICIONAIS NO PRODUTO
                    $_SESSION['carrinho']['produtos'][self::SearchProd($idprod)]['valoradicionais'] -= (number_format(str_replace("R$", "", str_replace(",",".",str_replace(" ","",$obProd->preco))), 2, '.', '')*$value);

                    }
                }
                //APAGA OS ADICIONAIS DO CARRINHO
                $_SESSION['carrinho']['produtos'][self::SearchProd($idprod)]['adicionais'] = [];

                if(sizeof($post) !==0) {
                //ADICIONA OS NOVOS PRODUTOS AO CARRINHO
                foreach ($post as $lanche) {
                    foreach($lanche as $key=>$value) {

                    //BUSCA O ADICIONAL NO BANCO DE DADOS
                    $obProd = Produto::getprodbyid($key);

                    //AUMENTA OU DIMINUI O VALOR TOTAL DO CARRINHO
                    $_SESSION['carrinho']['valortotal'] +=  number_format(str_replace("R$", "", str_replace(",",".",str_replace(" ","",$obProd->preco))), 2, '.', '') * (int)$value;

                    //AUMENTA OU DIMINUI O VALOR TOTAL DO PRODUTO JUNTAMENTE COM OS ADICIONAIS
                    $_SESSION['carrinho']['produtos'][self::SearchProd($idprod)]['valortotal'] += (number_format(str_replace("R$", "", str_replace(",",".",str_replace(" ","",$obProd->preco))), 2, '.', '')*(int)$value);

                    //AUMENTA OU DIMINUI O VALOR TOTAL DOS ADICIONAIS NO PRODUTO
                    $_SESSION['carrinho']['produtos'][self::SearchProd($idprod)]['valoradicionais'] += (number_format(str_replace("R$", "", str_replace(",",".",str_replace(" ","",$obProd->preco))), 2, '.', '')*(int)$value);

                    }

                    $_SESSION['carrinho']['produtos'][self::SearchProd($idprod)]['adicionais'] = array_filter($post);

                }

            }

            }

            unset($_POST['enviaradicional']);

            //RETORNA SUCESSO
            header('location: carrinho.php?status=success');
            exit;


          }

          //MÉTODO RESPONSÁVEL POR CANCELAR UMA COMPRA NA SESSÃO
          public static function CancelShop() {
            //APAGA CARRINHO
            unset($_SESSION['carrinho']);

            //REINICIA CARRINHO
            self::iniciarCarrinho();

            header('location: carrinho.php?status=success');
            exit;


          }


    }


?>