<?php 

namespace App\Entity;

use \App\Db\Database;
use \PDO;

class Contato {

    /**
     * Nome do usuário
     * @var string
     */
    public $nome;

    /**
     * Email do usuário
     * @var string
     */
    public $email;

    /**
     * Descrição feita pelo usuário
     * @var string
     */
    public $descricao;

    /**
      * Data e horário de cadastro do usuário
      * @var date
      */
    public $date;

    public function contatar() {

        //CONEXÃO COM O BANCO
        $obDatabase = new Database('contato');

        $this->date = date('Y-m-d H:i:s');

        //ARRAY COM OS VALORES A SEREM INSERIDOS NO BANCO
        $values = [
            //  o tipo estabelece o grau de hierarquia que o 
            //usuário irá ocupar no sistema, se é administrador ou usário comum
            'nome' => $this->nome,
            'email' =>$this->email,
            'descricao'  => $this->descricao,
            'datacadastro' => $this->date
        ];

        //CONEXÃO COM O BANCO PARA REALIZAR A INSERÇÃO DE DADOS
        $this->id = $obDatabase->insert($values);

        //RETORNA SUCESSO
        return true;
    }
}

?>