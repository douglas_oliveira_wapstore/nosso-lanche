<?php 


namespace App\Db;

use \PDO;
use \PDOException;


class Database {


    /**
     * CREDENCIAIS DE ACESSO AO BANCO DE DADOS
     */

    /**
     * Host de conexão com o banco
     */

     const HOST = 'db';

     /**
      * Senha de acesso ao banco de dados
      */
    const PASSWD = 'toor';

    /**
     * USUÁRIO DE ACESSO AO BANCO DE DADOS
     */
    const USER = 'root';

    /**
     * NOME DO BANCO DE DADOS A SER FEITA CONEXÃO
     */
    const NAME = 'tcc';

    /**
     * TABELA COM A QUAL SERÁ FEITA A CONEXÃO
     * @var string
     */
    private $table;

    /**
     * VARIÁVEL QUE SE CONECTA E SE COMUNICA COM O BANCO
     * @var PDO
     */

    private $connection;

    /**
     * função responsável por realizar a conexão com o banco de dados
     * @param string $table
     */
    public function __construct($table) {
        $this->table = $table;
        $this->setConnection();
    }

    /**
     * Método responsável por fazer a conexão com o banco de dados
     */

    public function setConnection() {
        try {
            $this->connection = new PDO("mysql:host=".self::HOST.";dbname=".self::NAME, self::USER, self::PASSWD);
            $this->connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
 
          } catch(PDOException $e) {
            echo "Erro de conexão: " . $e->getMessage();
          }
    }

    /**
     * Função responsável por executar comandos SQL
     * @param string $query
     * @param array $params 
     */

     public function execute($query, $params = []) {

        //PREPARA O COMANDO PARA SER EXECUTADO
        $stmt = $this->connection->prepare($query);

        //EXECUTA O COMANDO, COM OS VALORES DO BIND_PARAM COMO PARÂMETRO DO EXECUTE
        $stmt->execute($params);

        //RETORNA SUCESSO
        return $stmt;
     }

     /**
      * FUNÇÃO RESPONSÁVEL POR REALIZAR A INSERÇÃO DE DADOS DENTRO DO BANCO
      * @param array $values [fleids => values]
      * @return boolean
      */

      public function insert($fields = []) {

          
        //OBTÉM AS CHAVES E OS VALORES DO ARRAY EM OUTROS ARRAY SEPARADOS
        $values = array_values($fields);
        $keys = array_keys($fields);
    

        //ORGANIZA AS INTERROGAÇÕES DO BIND_PARAM QUERY
        $interrogations = '';
        foreach ($keys as $key => $value) {
            $interrogations .= ((int)$key !== (count($fields)) - 1) ? '?,' : '?';
                
        }

        //COMANDO DE INSERÇÃO COM O BANCO
        $query = 'INSERT INTO '. $this->table . ' ('.implode(',', $keys).') VALUES ('. $interrogations .')';

        //EXECUTA O COMANDO
        $this->execute($query, $values);

        //RETORNA SUCESSO
        return $this->connection->lastInsertId();

      }

      /**
       * FUNÇÃO RESPONSÁVEL POR RETORNAR DADOS DO BANCO
       * @param string $where,
       * @param string $order,
       * @param string $limit,
       * @param string $fields
       * @return boolean
       */

      public function select($where = null, $order = null, $limit = null, $fields = '*') {

            //ORGANIZA A QUERY BASEANDO-SE NOS FILTROS
            $where = isset($where) ? ' WHERE ' .$where : '';
            $order = isset($order) ? ' order by ' .$order : '';
            $limit = isset($limit) ? ' limit ' .$limit : '';

            //ESTABELECE A QUERY
            $query = 'SELECT '.$fields.' FROM '.$this->table.$where.$order.$limit;

            //EXECUTA A QUERY
            return $this->execute($query);


      }

      /**
       * Função responsável por atualizar os dados do banco
       * @param string $where
       * @param array $fields
       * @return boolean
       */
      public function update($where, $fields = []) {
        //SEPARA OS DADOS DO ARRAY
        $values = array_values($fields);
        $keys = array_keys($fields);

        //MONTA A QUERY
        $query = 'UPDATE '.$this->table.' SET '.implode('=?,',$keys).'=? WHERE ' . $where;

        //EXECUTA A QUERY
        $this->execute($query, $values);

        //RETORNA SUCESSO
        return true;
      }
    
      /**
       * Função responsável por remover dados do banco
       * @param string $where
       * 
       * @return boolean
       */

       public function delete($where) {

        //MONTA A QUERY
        $query = 'DELETE FROM '. $this->table . ' WHERE '. $where;

        //EXECUTA A QUERY
        $this->execute($query);

        //RETORNA SUCESSO
        return true;

       }


}


?>