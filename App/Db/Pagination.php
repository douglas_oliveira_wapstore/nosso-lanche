<?php

namespace App\Db;

class Pagination {

    /**
     * Variável que abriga o limite de dados por página
     * @var integer
     */
    private $limit;

    /**
     * Variável que abriga o número de páginas que a paginação terá
     * @var integer
     */
    public $pages;

    /**
     * Variável que abriga a quantidade de dados do banco
     * @var integer
     */
    private $dados;

    /**
     * Variável que retorna a página atual da paginação
     * @var integer
     */
    private $currentPage;

    /**
     * função responsável por atribuir os valores, realizando as devidas verificações
     * @param integer $dados
     * @param integer $limit
     * @param integer $currentPage
     */
    public function __construct($dados, $limit = 10, $currentPage = 1) {
        $this->dados = $dados;
        $this->limit = ($this->limit >= 0) ? $limit : 10;
        $this->currentPage = (is_numeric($currentPage)&&($currentPage)>0) ? $currentPage : 1;

        $this->calculate();
    }

    /**
     * método responsável por calcular a quantidade de páginas que a paginação terá
     */
    private function calculate() {
        $this->pages = ($this->dados > 0) ? ceil($this->dados / $this->limit) : 1;

        //VERIFICA SE A PÁGINA ATUAL É VÁLIDA
        $this->currentPage = ($this->currentPage <= $this->pages) ? $this->currentPage : $this->pages;
    }

    /**
     * Funçao responsável por calcular a parcela de dados que será retirada do banco de dados
     * @return string 
     */
    public function calculateLimit() {
        //CALCULA A PARTIR DE QUAL REGISTRO SERÁ CONTADO
        $start = ($this->currentPage - 1)*$this->limit;

        return $start . ',' . $this->limit;
    }

    /**
     * função responsável por organizar os guias da paginação (botões)
     * @return array
     */
    public function getPages() {

        $pages = [];

        //PARA PÁGINAS COM MENOS DE DOIS BOTÕES, A PAGINAÇÃO DEVERÁ SER DIFERENTE
        if($this->pages > 2)
            {    //PARA CADA PÁGINA, SERÁ VERIFICADO SE ELA É IGUAL À PÁGINA ATUAL
            for($i = 1; $i <= $this->pages; $i++) {

                //SE A PÁGINA ANALISADA FOR IGUAL À ATUAL, SERÁ ANALISADO QUAIS BOTÕES SERÃO COLOCADOS NA PAGINAÇÃO
                if($i == $this->currentPage) {
                                                //SE A PÁGINA ATUAL ESTIVER ENTRE O COMEÇO E O FIM DA PAGINAÇÃO, SERÁ EXIBIDO O BOTÃO DA PÁGINA ATUAL, O ANTECESSOR E O SUCESSOR DELE
                                                if ($i > 1 && $i < $this->pages){
                                                        for($j= $i - 1; $j<=$i+1; $j++) {
                                                            $pages[] = [
                                                                'pagina' => $j,
                                                                'atual' => $j == $this->currentPage
                                                            ];
                                                        }}
                                                // SE A PÁGINA ATUAL CORRESPONDER À PRIMEIRA, ELA SERÁ EXIBIDA JUNTAMENTE COM AS DUAS SUCESSORAS
                                                else if ($i == 1) {
                                                    for($j = $i; $j<=$i+2; $j++) {
                                                        $pages[] = [
                                                            'pagina' => $j,
                                                            'atual' => $j == $this->currentPage
                                                        ];
                                                    }
                                                }
                                                //SE A PÁGINA ATUAL FOR A ÚLTIMA, SERÁ EXIBIDO BOTÃO CORRESPONDENTE A ESTA JUNTAMENTE COM OS DOIS ANTECESSORES
                                                else if ($i == $this->pages) {
                                                    for($j = $i - 2; $j<=$i; $j++) {
                                                        $pages[] = [
                                                            'pagina' => $j,
                                                            'atual' => $j == $this->currentPage
                                                        ];
                                                        }
                                                    }

                                                }

                }
            }
        
        else {
            for($i = 1; $i <= $this->pages; $i++) {
                $pages[] = [
                    'pagina' => $i,
                    'atual' => $i == $this->currentPage
                ];
            }
        }    
            return $pages;

        }


    }




?>