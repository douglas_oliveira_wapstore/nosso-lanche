<?php 

    namespace App\Session;

    class Login {


        /**
         * Função responsável por iniciar sessão, caso esta ainda não esteja ativada
         */
        private static function init() {
            if(session_status() !== PHP_SESSION_ACTIVE) {
                session_start();
            }
        }

        /**
         * Método responsável por fazer o login da instância
         */
        public static function login($obUsuario) {
            //inicia a sessão   
            self::init();

            //estabelece a sessão
            $_SESSION['usuario'] = [
                'nome' => $obUsuario->nome,
                'email' => $obUsuario->email,
                'id' => $obUsuario->id,
                'tipo' => $obUsuario->tipo
            ];
            //ANALISA O TIPO DE USUÁRIO, REDIRECIONANDO-O PARA AS PÁGINAS CORRETAS
            switch($_SESSION['usuario']['tipo']) {
                case 'comum':
                    //REDIRECIONA PARA A PÁGINA DE USER COMUM
                    header('location: ./../home/index.php');
                    exit;

                case 'admin':
                    //REDIRECIONA PARA A PÁGINA DE ADMINISTRADOR
                    header('location: ./../home/index-admin.php');
                    exit;

                case 'moderador':
                    //REDIRECIONA PARA A PÁGINA DE FUNCIONÁRIO
                    header('location: ./../home/index-mod.php');
                    exit;
            }

        }
        /**
         * Método responsável por fazer o logout do usuário do sistema
         */

        public static function logout() {
            //INICIA A SESSÃO
            self::init();

            //DESABILITA A SESSÃO
            unset($_SESSION['usuario']);

            //DESABILITA A SESSÃO DE CARRINHO
            unset($_SESSION['carrinho']);
    

            //REDIRECIONA PARA A PÁGINA DE LOGIN
            header('location: ./../logincadastro/login.php');
            exit;
        }

        /**
         * Requere o login para executar determinada atividade
         */

         public static function requireLogin() {
             //INICIA A SESSÃO
            self::init();

            //REDIRECIONA, CASO O LOGIN NÃO TENHA SIDO FEITO
            if(!isset($_SESSION['usuario'])) {
                header('location: ./../logincadastro/login.php?login-required=');
                exit;
            }
         }

         /**
          * Requere logout para realizar determinada ação
          */

          public static function requireLogout() {
              //INICIA A SESSÃO
              self::init();

              if(isset($_SESSION['usuario'])) {
                  header('location: ./../home/index.php');
                  exit;
              }

          }

          /**
           * Método que retorna se o usuário está logado
           */

           public static function isLogged() {
               //INICIA A SESSÃO
               self::init();

               return isset($_SESSION['usuario']['id']);
           }

           /**
            * função que retorna os dados da sessão
            * @return array
            */

            public static function getUserSession() {
                //INICIA A SESSÃO
                self::init();

                return $_SESSION['usuario'] ?? null;
            }

            /**
             * Método responsável por fazer os redirects dos usuários, baseando-se em seus tipos
             */
            public static function redirect($tipo) {
                //INICA A SESSÃO
                self::init();

                //VERIFICA QUAL O TIPO DE USUÁRIO LOGADO PARA REDIRECIONÁ-LO
                if(isset($_SESSION['usuario'])) {
                    if($_SESSION['usuario']['tipo']!==$tipo)  {
                          switch($_SESSION['usuario']['tipo']) {
                              case 'admin':
                                  header('location: ./../home/index-admin.php');
                                  exit;
                        
                              case 'mod':
                                  header('location: ./../home/index-mod.php');
                                  exit;
                        
                              case 'comum':
                                  header('location: ./../home/index.php');
                                  exit;
                          }
                     }
                    
                 }

     }

}


?>