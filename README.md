# Nosso Lanche

Este projeto é um dos pré-requisitos para a aprovação no Curso Técnico em Informática realizado no Instituto Federal de São Paulo - Campus Birigui. O projeto se constitui um sistema de gerenciamento de recursos para um lanchonete da cidade.

Para mais detalhes concernentes à documentação, escopo e diagramas do sistema, acesse: [Projeto Final Curso Técnico](https://docs.google.com/document/d/17AmSW-7tgxl7fPd2s2uxQ1PZgwikAEmW/edit?usp=sharing&ouid=102025641665978763465&rtpof=true&sd=true)

Os autores do projeto são:

* Douglas Oliveira Paschoal
* Igor Kevenn Alencar Gomes
* João Gabriel Gomes Vieira
* Hemily Santana Dantas
* Leonardo Ferreira Roldão

## Getting started

Para o funcionamento do projeto, execute os seguintes passos:

* Execute os comandos de criação de banco de dados e tabelas para conexão com o back-end: [nossolanchetcc.sql](https://gitlab.com/douglas_oliveira_wapstore/nosso-lanche/-/blob/main/nossolanchetcc.sql)

* Execute o comando "composer install" para a importação de dependências utilizadas pelo projeto!

* Aproveite para pedir um lanche!

## Notas

* O projeto ainda não está 100% pronto, faltando algumas partes como a conexão com gateways de pagamento, dashboard de gerenciamento de pedidos, bem como outras operações no back. O desenvolvimento ocorreu até a finalização da venda e geração de nota da loja.

* Este é um dos primeiros projetos em PHP que desenvolvemos. Dessa maneira, serão necessárias algumas refatorações e revisões de código, que ficaram para implementações futuras.
