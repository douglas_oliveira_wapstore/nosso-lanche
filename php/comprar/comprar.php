<?php

require __DIR__.'./../../vendor/autoload.php';

use \App\Db\Database;
use \App\Entity\Usuario;
use \App\Session\Login;
use \App\Entity\Produto;
use \App\Db\Pagination;

    //OBJETO DO LOGIN
    $obLogin = Login::getUserSession();

    //EXIGE LOGIN
    Login::requireLogin();


    //FRASE DE BOAS VINDAS DO HEADER
    $welcome = isset($obLogin) ? '<p style="font-size: 13px;">Olá, '. $obLogin['nome'] . '. <a href="?acao=logout"  style="font-size: 13px;"><strong>Sair</strong></a></p>' : '<p style="font-size: 13px;">Olá, visitante. <a href="login.php"><strong>Entrar</strong></a></p>';

    if(isset($_GET['acao']) && $_GET['acao']=='logout') {
        Login::logout();
    }

    $obUser = Usuario::getUser('id = '.$_SESSION['usuario']['id']);


    //OBTÉM TODOS OS PRODUTOS DO BANCO
    $obProds = Produto::getProds();

    
    $obUser = Usuario::getUser('id = '.$_SESSION['usuario']['id']);


include __DIR__.'./../../includes/comprar/lista-compra.php';


?>
