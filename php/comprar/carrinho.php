<?php

require __DIR__.'./../../vendor/autoload.php';

use \App\Db\Database;
use \App\Entity\Usuario;
use \App\Session\Login;
use \App\Entity\Produto;
use \App\Db\Pagination;
use \App\Entity\Carrinho;


    //OBJETO DO LOGIN
    $obLogin = Login::getUserSession();

    //EXIGE LOGIN
    Login::requireLogin();


    //FRASE DE BOAS VINDAS DO HEADER
    $welcome = isset($obLogin) ? '<p style="font-size: 13px;">Olá, '. $obLogin['nome'] . '. <a href="?acao=logout" style="font-size: 13px;"><strong>Sair</strong></a></p>' : 'Olá, visitante. <a href="login.php"><strong>Entrar</strong></a>';

    //FAZ O LOGOUT
    if(isset($_GET['acao']) && $_GET['acao']=='logout') {
        Login::logout();
    }

    //OBTÉM TODOS OS ADITIVOS DO BANCO
    $obaditivo_lanche = Produto::getProds('tipo = "aditivo_lanche" AND disponivel = "s"', 'nome');
    $obaditivo_batata = Produto::getProds('tipo = "aditivo_batata" AND disponivel = "s"', 'nome');

 
    //OBTÉM TODOS OS LANCHES
    $lanches = array();

    //INICIA O CARRINHO, CASO NÃO ESTEJA INICIADO
    Carrinho::iniciarCarrinho();

    // print_r($_SESSION['carrinho']);
    // exit;


    //VERIFICA QUAIS PRODUTOS DA SESSÃO SÃO LANCHES
    foreach($_SESSION['carrinho']['produtos'] as $produto) {
        $teste = Produto::getprodbyid($produto['id']);

        if($teste->tipo == 'lanche') {
            $lanches[] = $produto;
        }
    }

    //REMOVE PRODUTOS DO CARRINHO
    if(isset($_GET['acaoproduto'])&&$_GET['acaoproduto'] == 'excluir') {
        if(!isset($_GET['keyproduto'])) {
            header('location: carrinho.php?status=error');
            exit;
        }

        Carrinho::deleteProd($_GET['keyproduto']);
    }

    $obUser = Usuario::getUser('id = '.$_SESSION['usuario']['id']);



    // //RECEBE AS INFORMAÇÕES DO MODAL DE ADITIVOS
    // if(isset($_POST['enviar'])) {
    //     print_r($_POST);
    //     exit;
    // }

    //CANCELA A COMPRA DA SESSÃO
    if(isset($_GET['cancela_carrinho'])) {
        Carrinho::CancelShop();
    }



    // //ENVIAR ADICIONAL NA SESSÃO
    // if(isset($_POST['enviaradicional'])) {

    //     Carrinho::addAdicional($_POST, $_POST['enviaradicional']);
    // }

    


include __DIR__.'./../../includes/comprar/lista-carrinho.php';


?>
