<?php 

require __DIR__.'./../../vendor/autoload.php';

use Dompdf\Options;
use \App\Entity\Usuario;
use \App\Session\Login;
use \App\Entity\Produto;

//INSTÂNCIA DE OPTIONS
$options = new Options();
$options->setChroot(__DIR__); //DEFINE A PASTA RAIZ COMO A PASTA ATUAL
$options->setIsRemoteEnabled(true); //PERMITE O CARREGAMENTO DE ARQUIVOS EXTERNOS NO SITE

// referenciando o namespace do dompdf

use Dompdf\Dompdf;

// instanciando o dompdf

$dompdf = new Dompdf($options);

//DADOS DA COMPRA
$forma_pagamento = $_POST['pagamento'];
$valor_pago_dinheiro = strlen($_POST['valor_dinheiro'])? number_format(str_replace(",",".",str_replace(" ","",$_POST['valor_dinheiro'])), 2, '.', '') : "";
$troco = isset($_POST['troco']) ? number_format(str_replace("R$", "", str_replace(",",".",str_replace(" ","",$_POST['troco']))), 2, '.', '') : "";

$informacoes_adicionais = $_POST['informacoes_adicionais'];

//DADOS DE ENTREGA
$endereco = $_POST['endereco'];
$numero_casa = $_POST['numero_house'];

$bairro = $_POST['bairro'];
$complemento = $_POST['complemento'] ?? '';

//DADOS DO USUÁRIO
$usuario = Login::getUserSession();
$obUser = Usuario::getUser('id = '.$usuario['id']);

//OBTÉM TODOS OS DADOS DA COMANDA
$produtos_carrinho = $_SESSION['carrinho']['produtos'];


//lendo o arquivo HTML correspondente

ob_start();

include './pdf/comanda-pdf.php';

$dompdf->loadHtml(ob_get_clean());

// $html = file_get_contents('./pdf/comanda-pdf.php');

// //inserindo o HTML que queremos converter

// $dompdf->loadHtml($html);


// Definindo o papel e a orientação

$dompdf->setPaper('A4', 'portrait');

// Renderizando o HTML como PDF

$dompdf->render();

// Enviando o PDF para o browser

// $dompdf->stream();
//IMPRIME CONTEÚDO DO ARQUIVO PDF NA TELA
header('Content-type: application/pdf');
echo $dompdf->output();




?>