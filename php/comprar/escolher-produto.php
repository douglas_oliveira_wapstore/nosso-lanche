<?php

require __DIR__.'./../../vendor/autoload.php';

use \App\Db\Database;
use \App\Entity\Usuario;
use \App\Session\Login;
use \App\Entity\Produto;
use \App\Db\Pagination;
use \App\Entity\Carrinho;


    //OBJETO DO LOGIN
    $obLogin = Login::getUserSession();

    //EXIGE LOGIN
    Login::requireLogin();

    //FRASE DE BOAS VINDAS DO HEADER
    $welcome = isset($obLogin) ? '<p style="font-size: 13px;">Olá, '. $obLogin['nome'] . '. <a href="?acao=logout"  style="font-size: 13px;"><strong>Sair</strong></a></p>' : '<p style="font-size: 13px;">Olá, visitante. <a href="login.php"><strong>Entrar</strong></a></p>';

    if(isset($_GET['acao']) && $_GET['acao']=='logout') {
        Login::logout();
    }




    $tipo = '';
    //VERFICA QUAL O TIPO DE PRODUTO QUE ESTÁ SENDO SOLICITADO PELO USUÁRIO
    if(isset($_GET['mostrar'])) {
        switch($_GET['mostrar']) {

            case 'lanches':
                $tipo = 'lanche';
                break;

            case 'bebidas':
                $tipo = 'bebida';
                break;

            case 'batatas':
                $tipo = 'batata';
                break;

            case 'outros':
                $tipo = 'outro';
                break;
        }
    }

    //REDIRECIONA À PÁGINA INICIAL, CASO  O NOME DO PRODUTO NÃO ESTEJA INFORMADO E CASO
    //NÃO TENHA SIDO SOLICITADO PARA ADICIONAR ALGUM INGREDIENTE
    if (!strlen($tipo)&&!isset($_POST['enviaradicional'])) {
        header('location: comprar.php?status=error');
        exit;
    }

    //ADICIONA O PRODUTO AO CARRINHO
    if(isset($_POST['produto'])) {

        
        Carrinho::addProd($_POST['produto'], $_POST['quantidade'] ?? null);

    }

    //ADICIONA INGREDIENTES AOS PRODUTOS
    if(isset($_POST['enviaradicional'])) {
 
        Carrinho::addAdicional($_POST, $_POST['enviaradicional']);
    }




    //OBTÉM TODOS OS PRODUTOS DO BANCO
    $obProds = Produto::getProds('tipo = "'.$tipo.'"', 'nome');

    
    $obUser = Usuario::getUser('id = '.$_SESSION['usuario']['id']);

include __DIR__.'./../../includes/comprar/lista-compra2.php';


?>
