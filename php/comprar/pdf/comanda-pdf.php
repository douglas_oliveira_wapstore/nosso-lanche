<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
    <title>Comanda Nosso Lanche</title>
    <head>
        <style>
            *{
                font-size: 12px;
                font-family: sans-serif;
            }
            .nobreak {
                page-break-inside: avoid;
            }
        </style>


<?php 

use \App\Entity\Produto;


$listagemcarrinho = '';

foreach($produtos_carrinho as $produto) {

//OBJETO DO PRODUTO
$obProduto = Produto::getprodbyid($produto['id']);

$listagemcarrinho .= '
<tr>
    <td>
        '.$produto['id'].'
    </td>
    <td>
        '.$produto["nome"].'
    </td>
    <td>
        '.(($obProduto->quantidade_unidade)*$produto['quantidade']) . ' '. (($produto['quantidade'] > 1) ? $obProduto->unidade . 's' : $obProduto->unidade).'
    </td>
    <td>
        R$' . $produto['valorunit'].'
    </td>
    <td>
        R$'. number_format(str_replace("R$", "", str_replace(",",".",str_replace(" ","",($produto['valortotal'] - $produto['valoradicionais'])))), 2, '.', '') . '
    </td>
</tr>
';

}



$listagemadicionais = '';
$totaladicionais = 0;

foreach($produtos_carrinho as $produto) {
    $totaladicionais += number_format(str_replace("R$", "", str_replace(",",".",str_replace(" ","",$produto['valoradicionais']))), 2, '.', '');

    if($produto['adicionais'] == null) {
        $produto['adicionais'] = array();
    }

    foreach($produto['adicionais'] as $listagemadicional){

        $contador = 0;
        foreach($listagemadicional as $adicional=>$quantidade) {
        
        $contador++;

        $obAdicional = Produto::getprodbyid($adicional);
        
         $listagemadicionais .= '
             <tr>'.(($contador == 1) ? '
                <td rowspan='.count($listagemadicional).'>
                    '.$produto['id'].'
                </td>
                 <td rowspan='.count($listagemadicional).'>
                     '.$produto['nome'].'
                 </td>' : '').'
                 <td>
                     '.$obAdicional->nome.'
                 </td>
                 <td>
                     '.(((int)$obAdicional->quantidade_unidade * (int)$quantidade) .' ' . (($quantidade > 1) ? $obAdicional->unidade . 's' : $obAdicional->unidade)).'
                 </td>
                 <td>
                     R$'.number_format(str_replace("R$", "", str_replace(",",".",str_replace(" ","",$obAdicional->preco))), 2, '.', '')*$quantidade.'
                 </td>
             </tr>
      

         ';
        }
    }

}





?>


</head>
<body>
    <img src="./pdf/img/icon.jpg" alt="icon" border="0" width="350px">

    <h1 style="text-align: center;"> Nota de compra</H2>
    <h3 style="text-align: center;color: gray;">ID: 0151</h3>


    <table class="table table-bordered">
            <thead>
                <tr><th colspan="4" style="text-align: center;">Dados do usuário</th></tr>
            </thead>
            <tr>
                <td><b>Nome:</b></td>
                <td><?=$obUser->nome?></td>
                <td><b>CPF:</b></td>
                <td><?=$obUser->cpf?></td>
            </tr>
            <tr>
                <td><b>E-mail:</b></td>
                <td><?=$obUser->email?></td>
                <td><b>Telefone:</b></td>
                <td><?=$obUser->celular?></td>
            </tr>
            <tr>
                <td><b>Endereço p/ entrega:</b></td>
                <td><?=$endereco?>,nº <?=$numero_casa?></td>
                <td><b>Bairro: </b></td>
                <td><?=$bairro?></td>
            </tr>
            <tr>
                <td><b>Complemento</b></td>
                <td><?=$complemento?></td>
            </tr>



    </table>
    
<!-- 


    Nome
    CPF
    Endereço
    Telefone
    ID da compra
    ID do usuário
    

    Tabela

    Sub-total
    Total

    Forma de pagamento
    Troco
    Data da compra

 -->

    <table class="table table-striped mt-1">
        <thead class="thead-dark">

            <tr>
                <th scope="col">ID</th>
                <th scope="col">Produto</th>
                <th scope="col">Preço unitário</th>
                <th scope="col">Quantidade</th>

                <th scope="col">Total</th>
            </tr>
        <tbody>
            <?=$listagemcarrinho?>
            <tr class="table-dark text-dark">
                <td colspan="4"><b>Total dos produtos  ...........................................................................................................................</b></td>
                <td><b>R$ <?=number_format(str_replace("R$", "", str_replace(",",".",str_replace(" ","",($_SESSION['carrinho']['valortotal'] - $totaladicionais)))), 2, '.', '')?></b></td>
            </tr>

        </tbody>

        </thead>

    </table>
    <br>

    <label for="">Informações adicionais:</label>
    <input type="text" value="<?=$informacoes_adicionais?>" class="form-control" style="height: 150px;">

<br>

    <div class="nobreak">
    <table class="table table-striped">

    <thead class="thead-dark">
        <tr>
            <th>ID Prod.</th>
            <th>Produto</th>

            <th>Adicional</th>
            <th>Quantidade</th>
            <th>Sub-total</th>

        </tr>

    </thead>
        <?=$listagemadicionais?>
        <tr class="table-dark text-dark">
            <td colspan="4"><b>Total dos adicionais  .................................................................................................</b></td>
            <td><b>R$ <?=number_format(str_replace("R$", "", str_replace(",",".",str_replace(" ","",$totaladicionais))), 2, '.', '')?></b></td>
        </tr>

    </table>

    
    <table class="table table-bordered">
        <thead>
            <tr><th colspan="4" style="text-align: center;">Dados de pagamento</th></tr>
        </thead>
        <tbody>
            <tr>
                <td><b>Sub-total: </b></td>
                <td>R$ <?=number_format(str_replace("R$", "", str_replace(",",".",str_replace(" ","",$_SESSION['carrinho']['valortotal']))), 2, '.', '')?></td>
                <td><b>Taxa de entrega:</b></td>
                <td>R$ 3.00</td>
            </tr>
            <tr>
                <td><b>Total:</b></td>
                <td>R$ <?=number_format(str_replace("R$", "", str_replace(",",".",str_replace(" ","",$_SESSION['carrinho']['valortotal']))), 2, '.', '') + 3.00?></td>
                <td><b>Forma de pagamento:</b></td>
                <td><?=$forma_pagamento?></td>
            </tr>
            <tr>
                <td><b>Valor no dinheiro: </b></td>
                <td><?=$valor_pago_dinheiro?></td>
                <td><b>Troco para: </b></td>
                <td><?=$troco?></td>
            </tr>
        </tbody>
    </table>
</div>

    
</body>
</html>