<?php 

require __DIR__ . './../../vendor/autoload.php';
use \App\Db\Database;
use \App\Entity\Usuario;
use \App\Session\Login;

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

// REQUERE O LOGOUT
Login::requireLogout();

//REDIRECIONA OS TIPOS DE USUÁRIO PARA AS PÁGINAS CORRETAS



//ALERTAS DE ERROS DO SISTEMA
$alertaLogin = '';
$alertaSenha = '';
$alertaEmail ='';

//VERIFICA SE O BOTÃO INSERIDO FOI O DE CADASTRO OU DE LOGIN
if(isset($_POST['acao'])) {
    switch($_POST['acao']) {
        case 'cadastrar':
        if(isset(   $_POST['nome'],
                    $_POST['email'],
                    $_POST['cpf'],
                    $_POST['cidade'],
                    $_POST['estado'],
                    $_POST['celular'],
                    $_POST['senha'],
                    $_POST['senha2']
                )) {
                    //VERIFICA SE A SENHA INSERIDA É IGUAL À REPETIÇÃO DELA
                    if($_POST['senha'] == $_POST['senha2']) {
                        
                        $obUser = new Usuario();
                        //ATRIBUI OS ATRIBUTOS DO $_GET AO OBJETO
                        $obUser->nome = $_POST['nome'];
                        $obUser->email = $_POST['email'];
                        $obUser->cpf = $_POST['cpf'];
                        $obUser->cidade = $_POST['cidade'];
                        $obUser->estado = $_POST['estado'];
                        $obUser->celular = $_POST['celular'];
                        $obUser->tipo = 'comum';
                        $obUser->senha = password_hash($_POST['senha'], PASSWORD_DEFAULT);

                        $obUser2 = Usuario::getUser('email = "'.$_POST['email'].'"');
                     

                        //VERIFICA SE O EMAIL CADASTRADO JÁ EXISTE NO SISTEMA
                        if (!$obUser2 instanceof Usuario) {
                        //CADASTRA O USUÁRIO
                        $obUser->cadastrar();

                        //REALIZA O LOGIN DO USUÁRIO LOGO APÓS O CADASTRO
                        Login::login($obUser);
                        break;
                        }
                        else {
                            $alertaEmail = '<div class="alert-danger">O email preenchido já está em uso.</div>';
                            break;
                        }
                    }

                    else{
                        $alertaSenha = '<div class="alert-danger">As senhas não conferem.</div>';
                        break;
                    } 


        }

        case 'logar':
        
        if (isset($_POST['email'],$_POST['senha'])) {
            $obUser = Usuario::getUser('email = "'.$_POST['email'].'"');

            //VERIFICA SE O EMAIL OU SENHA ESTÃO CORRETOS
            if (!$obUser instanceof Usuario || !password_verify($_POST['senha'], $obUser->senha)) {
                $alertaLogin = '<div class="alert-danger">Usuário ou senha está incorreto.</div>';
                unset($_POST['acao']);
                break;
            }

            //REALIZA O LOGIN DO USUÁRIO
            Login::login($obUser);
            break;

            
        }
        
    }
}



//VERIFICA SE FOI SOLICITADO QUE O USUÁRIO FIZESSE LOGIN
if(isset($_GET['login-required'])) {
    $alertaLogin = '<div class="alert-danger">É necessário fazer login!</div>';
}



include __DIR__.'./../../includes/login/form-login.php';


?>