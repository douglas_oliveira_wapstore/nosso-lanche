<?php 

require __DIR__.'./../../vendor/autoload.php';

use \App\Db\Database;
use \App\Entity\Produto;
use \App\Session\Login;

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);


    //TEXTO DO TÍTULO E DOS BOTÕES
    $titulo = 'CADASTRAR PRODUTO';
    $btn = 'Cadastrar';

    //EXIGE LOGIN
    Login::requireLogin();

    //OBJETO DO LOGIn
    $obLogin = Login::getUserSession();
    
    //REDIRECIONA O USUÁRIO PARA A PÁGINA CERTA, CASO NÃO ESTEJA DE ACORDO COM O TIPO ADMIN
    Login::redirect('admin');

    //FRASE DE BOAS VINDAS DO HEADER
    $welcome = isset($obLogin) ? '<p style="font-size: 13px;">Olá, '. $obLogin['nome'] . '. <a href="cadastro-prod.php?acao=logout"  style="font-size: 13px;"><strong>Sair</strong></a></p>' : '<p style="font-size: 13px;">Olá, visitante. <a href="login.php"><strong>Entrar</strong></a></p>';

    //FAZ LOGOUT
    if(isset($_GET['acao']) && $_GET['acao']=='logout') {
            Login::logout();
    }

    $obProd = new Produto();

    //RECUPERA OS DADOS INSERIDOS NO FORMULÁRIO
    if(isset($_POST['cadastrar'])) {
        if(isset($_POST['nome'],$_POST['preco'],$_POST['descricao'],$_POST['tipo'], $_POST['disponivel'])) {
 

            //ATRIBUI OS VALORES DO FORMULÁRIO
            $obProd->nome = $_POST['nome'];
            $obProd->preco = $_POST['preco'];
            $obProd->descricao = $_POST['descricao'];
            $obProd->tipo = $_POST['tipo'];
            $obProd->disponivel = $_POST['disponivel'];
            $obProd->quantidade_unidade = $_POST['quantidade_unidade'];
            $obProd->unidade = $_POST['unidade'];

            //REALIZA O CADASTRO
            $obProd->cadastrar();

            //REDIRECIONA COM O COMANDO DE SUCESSO
            header('location: produtos.php?status=success');
            exit;
        }
    }
    

include __DIR__.'./../../includes/produtos/form-cad-prod.php';


?>