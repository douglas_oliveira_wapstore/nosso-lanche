<?php 

require __DIR__.'./../../vendor/autoload.php';

use \App\Db\Database;
use \App\Entity\Produto;
use \App\Session\Login;

    //TEXTO DO TÍTULO E DOS BOTÕES
    $titulo = 'EDITAR PRODUTO';
    $btn = 'Alterar';

    //EXIGE LOGIN
    Login::requireLogin();

    
    //OBJETO DO LOGIn
    $obLogin = Login::getUserSession();
    
    //REDIRECIONA O USUÁRIO PARA A PÁGINA CERTA, CASO NÃO ESTEJA DE ACORDO COM O TIPO ADMIN
    Login::redirect('admin');

    
    //FRASE DE BOAS VINDAS DO HEADER
    $welcome = isset($obLogin) ? '<p style="font-size: 13px;">Olá, '. $obLogin['nome'] . '. <a href="editar-prod.php?logout=logout"  style="font-size: 13px;"><strong>Sair</strong></a></p>' : '<p style="font-size: 13px;">Olá, visitante. <a href="login.php"><strong>Entrar</strong></a></p>';

    //FAZ LOGOUT
    if(isset($_GET['logout']) && $_GET['logout']=='logout') {
            Login::logout();
            exit;
    }

    //REALIZA AS VERIFICAÇÕES DO ID
    if(!isset($_GET['id']) || !is_numeric($_GET['id'])) {
        header('location: produtos.php?status=error');
        exit;
    }

    //OBJETO QUE FILTA O PRODUTO BASEADO EM SEU ID
    $obProd = Produto::getprodbyid($_GET['id']);

    //REALIZA AS VERIFICAÇÕES DO $_GET
    if(!$obProd instanceof Produto) {
        header('location: produtos.php?status=error');
        exit;
    }




    $obProd2 = new Produto();

    //ALTERA OS DADOS DO FORMULÁRIO
    if(isset($_POST['cadastrar'])) {
        if(isset($_POST['nome'],$_POST['preco'],$_POST['descricao'],$_POST['tipo'], $_POST['disponivel'])) {
 

            //ATRIBUI OS VALORES DO FORMULÁRIO
            $obProd2->nome = $_POST['nome'];
            $obProd2->preco = $_POST['preco'];
            $obProd2->descricao = $_POST['descricao'];
            $obProd2->tipo = $_POST['tipo'];
            $obProd2->disponivel = $_POST['disponivel'];
            $obProd2->quantidade_unidade = $_POST['quantidade_unidade'];
            $obProd2->unidade = $_POST['unidade'];

            //REALIZA O CADASTRO
            $obProd2->atualizar($_GET['id']);

            //REDIRECIONA COM O COMANDO DE SUCESSO
            header('location: produtos.php?status=success');
            exit;
        }
    }
 
 


    

include __DIR__.'./../../includes/produtos/form-cad-prod.php';


?>