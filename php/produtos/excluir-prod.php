<?php 

require __DIR__.'./../../vendor/autoload.php';

use \App\Db\Database;
use \App\Entity\Produto;
use \App\Session\Login;

    //TEXTO DO TÍTULO E DOS BOTÕES
    $titulo = 'EDITAR PRODUTO';
    $btn = 'Alterar';

    //EXIGE LOGIN
    Login::requireLogin();

        //OBJETO DO LOGIn
        $obLogin = Login::getUserSession();
    
        //REDIRECIONA O USUÁRIO PARA A PÁGINA CERTA, CASO NÃO ESTEJA DE ACORDO COM O TIPO ADMIN
        Login::redirect('admin');
    
        //FRASE DE BOAS VINDAS DO HEADER
        $welcome = isset($obLogin) ? '<p style="font-size: 13px;">Olá, '. $obLogin['nome'] . '. <a href="excluir-prod.php?acao=logout"  style="font-size: 13px;"><strong>Sair</strong></a></p>' : '<p style="font-size: 13px;">Olá, visitante. <a href="login.php"><strong>Entrar</strong></a></p>';
    
        //FAZ LOGOUT
        if(isset($_GET['acao']) && $_GET['acao']=='logout') {
                Login::logout();
        }

    //REALIZA AS VERIFICAÇÕES DO ID
    if(!isset($_GET['id']) || !is_numeric($_GET['id'])) {
        header('location: produtos.php?status=error');
        exit;
    }

    //OBJETO QUE FILTA O PRODUTO BASEADO EM SEU ID
    $obProd = Produto::getprodbyid($_GET['id']);

    //REALIZA AS VERIFICAÇÕES DO $_GET
    if(!$obProd instanceof Produto) {
        header('location: produtos.php?status=error');
        exit;
    }



    if(isset($_POST['remover'])) {
        Produto::excluir($_GET['id']);
        header('location: produtos.php?status=success');
        exit;
    }


    

include __DIR__.'./../../includes/produtos/confirma-exclusao.php';


?>