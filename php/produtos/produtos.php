<?php 

require __DIR__ . './../../vendor/autoload.php';

use \App\Db\Database;
use \App\Entity\Usuario;
use \App\Session\Login;
use \App\Entity\Produto;
use \App\Db\Pagination;

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);


    //OBJETO DO LOGIN
    $obLogin = Login::getUserSession();

    //EXIGE LOGIN
    Login::requireLogin();
    
    //REDIRECIONA O USUÁRIO PARA A PÁGINA CERTA, CASO NÃO ESTEJA DE ACORDO COM O TIPO
    Login::redirect('admin');
    
    //FRASE DE BOAS VINDAS DO HEADER
    $welcome = isset($obLogin) ? '<p style="font-size: 13px;">Olá, '. $obLogin['nome'] . '. <a href="produtos.php?acao=logout"  style="font-size: 13px;"><strong>Sair</strong></a></p>' : '<p style="font-size: 13px;">Olá, visitante. <a href="login.php"><strong>Entrar</strong></a></p>';

    if(isset($_GET['acao']) && $_GET['acao']=='logout') {
        Login::logout();
    }


    //OBTÉM A QUANTIDADE DE DADOS QUE EXISTEM NO BANCO

    //OBJETO DA PAGINAÇÃO

    //OBTÉM DADOS DO FILTRO DA URL
    $id = filter_input(INPUT_GET, 'id', FILTER_SANITIZE_STRING);
    $nome = filter_input(INPUT_GET, 'nome', FILTER_SANITIZE_STRING);
    $tipo = filter_input(INPUT_GET, 'tipo', FILTER_SANITIZE_STRING);
    $disponivel = filter_input(INPUT_GET, 'disponivel', FILTER_SANITIZE_STRING);

    //ARRAY CONTENDO OS DADOS DO FILTRO
    $busca = [
        strlen($nome) ? 'nome LIKE "%'.str_replace(' ','%',$nome).'%"' : null,
        strlen($id) ? 'id ='. $id : null,
        strlen($tipo) ? 'tipo= "'.$tipo .'"': null,
        strlen($disponivel) ? 'disponivel ="'.$disponivel.'"' : null
    ];

    //MONTA O COMANDO DE BUSCA DO FILTRO
    $filtro = implode(' AND ', array_filter($busca));


    //VARIÁVEL QUE ABRIGA A QUANTIDADE PRODUTOS QUE EXISTEM NO BANCO DE DADOS
    $quantidadeProds = Produto::getQuantidadeProdutos(strlen($filtro)? $filtro:null);

    //OBJETO DA PAGINAÇÃO   
    $obPagination = new Pagination($quantidadeProds, 20, $_GET['pagina'] ?? 1);

    //VARIÁVEL QUE CALCULA O LIMITE DE DADOS DA PAGINAÇÃO
    $offsetlimit = $obPagination->calculateLimit();

    //OBTÉM TODOS OS PRODUTOS DO BANCO
    $obProds = Produto::getProds(strlen($filtro)? $filtro:null, 'nome', $offsetlimit);

    //ARRAY QUE ORGANIZA A PAGINAÇÃO
    $pages = $obPagination->getPages();

include __DIR__.'./../../includes/produtos/listagem-prods.php';


?>