<?php


require __DIR__.'./../../vendor/autoload.php';

use \App\Db\Database;
use \App\Entity\Produto;
use \App\Session\Login;
use \App\Entity\Usuario;

    //TEXTO DO TÍTULO E DOS BOTÕES
    $titulo = 'EDITAR PRODUTO';
    $btn = 'Alterar';

    //EXIGE LOGIN
    Login::requireLogin();

        //OBJETO DO LOGIn
        $obLogin = Login::getUserSession();
    
        //REDIRECIONA O USUÁRIO PARA A PÁGINA CERTA, CASO NÃO ESTEJA DE ACORDO COM O TIPO ADMIN
        Login::redirect('admin');
    
        //FRASE DE BOAS VINDAS DO HEADER
        $welcome = isset($obLogin) ? '<p style="font-size: 13px;">Olá, '. $obLogin['nome'] . '. <a href="?acao=logout"  style="font-size: 13px;"><strong>Sair</strong></a></p>' : '<p style="font-size: 13px;">Olá, visitante. <a href="login.php"><strong>Entrar</strong></a></p>';
    
        //FAZ LOGOUT
        if(isset($_GET['acao']) && $_GET['acao']=='logout') {
                Login::logout();
        }

    //REALIZA AS VERIFICAÇÕES DO ID
    if(!isset($_GET['id']) || !is_numeric($_GET['id'])) {
        header('location: usuarios.php?status=error');
        exit;
    }

    //OBJETO QUE FILTA O PRODUTO BASEADO EM SEU ID
    $obUser = Usuario::getUser('id ='.$_GET['id']);

    //REALIZA AS VERIFICAÇÕES DO $_GET
    if(!$obUser instanceof Usuario) {
        header('location: usuarios.php?status=error');
        exit;
    }



    if(isset($_POST['remover'])) {
        Usuario::excluir($_GET['id']);
        header('location: usuarios.php?status=success');
        exit;
    }




include __DIR__.'./../../includes/usuarios/confirma-exclusao-user.php';


?>
