<?php

require __DIR__.'./../../vendor/autoload.php';

use \App\Db\Database;
use \App\Entity\Usuario;
use \App\Session\Login;
use \App\Entity\Produto;
use \App\Db\Pagination;
use \App\Entity\Carrinho;


    //OBJETO DO LOGIN
    $obLogin = Login::getUserSession();

    //EXIGE LOGIN
    Login::requireLogin();

    //REDIRECIONA O USUÁRIO PARA A PÁGINA CERTA, CASO NÃO ESTEJA DE ACORDO COM O TIPO
    Login::redirect('admin');

    //FRASE DE BOAS VINDAS DO HEADER
    $welcome = isset($obLogin) ? '<p style="font-size: 13px;">Olá, '. $obLogin['nome'] . '. <a href="?acao=logout" style="font-size: 13px;"><strong>Sair</strong></a></p>' : 'Olá, visitante. <a href="login.php"><strong>Entrar</strong></a>';

    //FAZ O LOGOUT
    if(isset($_GET['acao']) && $_GET['acao']=='logout') {
        Login::logout();
    }

    if(!isset($_GET['id'])||!is_numeric($_GET['id'])) {
        header('location: usuarios.php?status=error');
        exit;
    }

    if(isset($_GET['id'])) {
    
        $obUser = Usuario::getUser('id ='.$_GET['id']);
    }

    if(!$obUser instanceof Usuario) {
        header("location: usuarios.php?status=error");
        exit;
    }



include __DIR__.'./../../includes/usuarios/dados-cliente.php';


?>
