<?php 

require __DIR__ . './../../vendor/autoload.php';

use \App\Db\Database;
use \App\Entity\Usuario;
use \App\Session\Login;
use \App\Db\Pagination;

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);


    //OBJETO DO LOGIN
    $obLogin = Login::getUserSession();

    //EXIGE LOGIN
    Login::requireLogin();
    
    //REDIRECIONA O USUÁRIO PARA A PÁGINA CERTA, CASO NÃO ESTEJA DE ACORDO COM O TIPO
    Login::redirect('admin');
    
    //FRASE DE BOAS VINDAS DO HEADER
    $welcome = isset($obLogin) ? '<p style="font-size: 13px;">Olá, '. $obLogin['nome'] . '. <a href="usuarios.php?acao=logout"  style="font-size: 13px;"><strong>Sair</strong></a></p>' : '<p style="font-size: 13px;">Olá, visitante. <a href="login.php"><strong>Entrar</strong></a></p>';

    if(isset($_GET['acao']) && $_GET['acao']=='logout') {
        Login::logout();
    }



    //RECEBE O COMANDO DE TORNAR MODERADOR OU COMUM UM USUÁRIO
    //SE O GET DO TIPO FOR COMUM, ELE SERÁ TORNADO MOD
    //SE O GET DO TIPO FOR MOD, ELE SERÁ TORNADO COMUM

    if(isset($_GET['change'])) {
        $obUser = Usuario::getUser('id ='.$_GET['id']);

        switch($_GET['change']) {
            case 'mod':
                $obUser->tipo = 'comum';
                $obUser->atualizar($_GET['id']);
                header('location: usuarios.php?status=success');
                exit;

            case 'comum':
                $obUser->tipo = 'mod';
                $obUser->atualizar($_GET['id']);
                header('location: usuarios.php?status=success');
        }
    }

    //RETIRA OS DADOS DO FILTRO DIRETAMENTE DA URL
    $id = filter_input(INPUT_GET, 'id', FILTER_SANITIZE_STRING);
    $nome = filter_input(INPUT_GET, 'nome', FILTER_SANITIZE_STRING);
    $tipo = filter_input(INPUT_GET, 'tipo', FILTER_SANITIZE_STRING);
    $telefone = filter_input(INPUT_GET, 'telefone', FILTER_SANITIZE_STRING);

    //ARRAY COM O COMANDO DE BUSCA DO FILTRO
    $busca = [
        strlen($id) ? 'id = '.$id : null,
        strlen($nome) ? 'nome LIKE "%'.str_replace(' ', '%', $nome). '%"' : null,
        strlen($tipo) ? 'tipo = "'.$tipo.'"' : null,
        strlen($telefone) ? 'celular LIKE "%'.$telefone.'%"' : null
    ];

    //MONTA O COMANDO DE BUSCA DO FILTRO
    $queryfiltro = implode(' AND ', array_filter($busca));



    //VARIÁVE QUE ABRIGA A QUANTIDADE DE REGISTROS QUE EXISTEM NO BANCO DE DADOS
    $dataQuantity = Usuario::getQuantidadeUsers(strlen($queryfiltro) ? $queryfiltro : null);

    //OBJETO DA PAGINAÇÃO
    $obPagination = new Pagination($dataQuantity, 20, $_GET['pagina'] ?? 1);

    //VARIÁVEL QUE ABRIGA O LIMITE DE DADOS QUE DEVEM SER EXIBIDOS POR PÁGINA
    $limit = $obPagination->calculateLimit();

    //VARIÁVEL QUE ORGANIZA OS GUIAS DE PAGINAÇÃO
    $pages = $obPagination->getPages();

        
    //VARIÁVEL QUE ABRIGA TODOS OS USUÁRIOS CADASTRADOS NO BANCO DE DADOS
    $users = Usuario::getUsers(strlen($queryfiltro) ? $queryfiltro : null, 'nome', $limit);

include __DIR__.'./../../includes/usuarios/listagem-users.php';


?>