<?php
require __DIR__ . './../../vendor/autoload.php';

use \App\Db\Database;
use \App\Entity\Usuario;
use \App\Session\Login;

    //VERIFICA SE O USUÁRIO ESTÁ LOGADO
    Login::requireLogin();

    //OBJETO DO LOGIn
    $obLogin = Login::getUserSession();
    
    //REDIRECIONA O USUÁRIO PARA A PÁGINA CERTA, CASO NÃO ESTEJA DE ACORDO COM O TIPO
    Login::redirect('admin');
    
    //FRASE DE BOAS VINDAS DO HEADER
    $welcome = isset($obLogin) ? '<p style="font-size: 13px;">Olá, '. $obLogin['nome'] . '. <a href="index-admin.php?acao=logout"  style="font-size: 13px;"><strong>Sair</strong></a></p>' : '<p style="font-size: 13px;">Olá, visitante. <a href="login.php"><strong>Entrar</strong></a></p>';

    if(isset($_GET['acao']) && $_GET['acao']=='logout') {
        Login::logout();
    }


include __DIR__.'./../../includes/home/pagina-principal-admin.php';
?>