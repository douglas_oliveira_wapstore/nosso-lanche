<?php 

require __DIR__ . './../../vendor/autoload.php';

use \App\Db\Database;
use \App\Entity\Usuario;
use \App\Session\Login;


ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);


    //OBJETO DO LOGIN
    $obLogin = Login::getUserSession();
    
    //REDIRECIONA O USUÁRIO PARA A PÁGINA CERTA, CASO NÃO ESTEJA DE ACORDO COM O TIPO
    Login::redirect('comum');
    
    //FRASE DE BOAS VINDAS DO HEADER
    $welcome = isset($obLogin) ? '<p style="font-size: 13px;">Olá, '. $obLogin['nome'] . '. <a href="index.php?acao=logout"  style="font-size: 13px;"><strong>Sair</strong></a></p>' : '<p style="font-size: 13px;">Olá, visitante. <a href="./../logincadastro/login.php"><strong>Entrar</strong></a></p>';

    if(isset($_GET['acao']) && $_GET['acao']=='logout') {
        Login::logout();
    }


include __DIR__.'./../../includes/home/pagina-principal.php';


?>